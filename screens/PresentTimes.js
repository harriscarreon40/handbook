import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class PresentImes extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
            <Left>
              <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
                this.props.navigation.openDrawer()} />
            </Left>
            <Body>
              <Text>Present Times</Text>
            </Body>
            <Right>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                  onPress={() => navigate('University History')}               
                />
              </Button>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                  onPress={() => navigate('University Symbols')}               
                />
              </Button>
            </Right>
        </Header>

        <ScrollView>
          <View style={[styles.container]} >
            <Text>
              {'\t'} {'\t'} To align with the demands of the current market and of the Philippine economy and now the ASEAN integration, the university, through the different colleges in its five campuses, is now offering 63 academic programs - three for Doctoral degrees, 27 for Masters degrees and 33 for Baccalaureate programs - in the fields of Engineering, Education, Arts, Sciences, Economics, Computing, Governance, Development, Resources Management, Technology, Agriculture and Forestry. {'\n'} {'\n'}
              
              {'\t'} {'\t'} Beginning 2002, enthused by the desire to impart quality education, USeP beganto voluntarily submit itself to accreditation exercises. At present, many of the academic programs offered in the university have attained Accredited Status by the Accrediting Agency of Chartered Colleges and Universities in the Philippines (AACCUP). As of 2016 ten programs are Level I accredited, 18 are Level II accredited and six are Level III accredited. Among those accredited as Level III are Bachelor of Elementary Education, Bachelor of Secondary Education, Bachelor of Computer Technology, Bachelor of Industrial Technology, Bachelor Technical Teacher Education and BS in Biology, all of which are offered in the Obrero campus. {'\n'} {'\n'}

              {'\t'} {'\t'} To fulfill its vision to become a modern state university at the cutting edge of academic excellence and at the forefront of research and development, the university continues to encourage its faculty members to pursue further studies and research. Majority or 54 percent of the teaching force have Master's degrees, 29 percent have Bachelor's degrees while the remaining 17 percent possess Doctoral degrees. Moreover, the outstanding performance shown by USeP Engineering graduates during board examinations instigated the Commission on Higher Education (CHED) to declare USeP as a Center of Development in Engineering, specifically in Electrical Engineering (EE) and Electronics and Communication Engineering (ECE). {'\n'} {'\n'}

              {'\t'} {'\t'} In order to contribute markedly to maximize the use of Mindanao's potential resources for the well being of its people, the Research Division of the university has always worked to hold research and development projects relevant to the university's academic programs which includes food productivity and sustainability, technology, industrial fields, nutrition, health and resources management. Also, USeP is now the base agency of Southern Mindanao Agriculture and Resources Research and Development Consortium (SMARRDEC), a member consortium of line agencies and research institutions in agriculture and agri-related research and development. {'\n'} {'\n'}

              {'\t'} {'\t'} Similarly, the Extension Division has incessantly undertaken programs and outreach services in the communities through the establishment of agriculture and fishery projects, capability-building programs, computer literacy and education, livelihood videotape showing, household skills training and others. Realizing that endeavors would be best met through convergence and partnerships, the university has explored means to establish linkages. To date, the university has inked partnerships with World Bank, Philippines-Australia Basic Education Assistance for Mindanao, Southeast Asian Ministries of Education Organization, Regional Center for Vocational and Technical
              Education and Training in Brunei Darussalam (SEAMEO VOCTECH), Philippines-Australia Human Resource Development Facility, Department of Education, Department of Agriculture, Department of Science and Technology, PhilRice, National Computer Center, Department of Energy, National Commission for Culture and the Arts, Resources for the Blind, Inc., SMART Philippines, SMARRDEC, Southern Mindanao Industry,
              Energy, Research and Development Consortium (SMIERDEC), DARUMA Tech, Inc., PCAMMRD, Mindanao Studies Consortium, Davao City Colleges and Universities Network (DACUN), and MSTPC. Today, new centers that are meant to be strategic growth points for USeP have been established. These are the Teacher Training Center for Region XI (Mindanao), the Mindanao eLearning Space (a project with DepEd and BEAM), the Science and Technology Learning Resource Center, the Regional Trades and Crafts Training and Production Center, the Knowledge for Development Center (with World Bank) and the Lifelong Study Center. With USeP avidly pursuing its USeP6 Strategic Institutional Repositioning Project, the USeP community and stakeholders are optimistic that the university will effectively fulfill its mission in this part of the country. {'\n'} {'\n'}

              {'\t'} {'\t'} It is also worthwhile to note that because of the quality of performance of its alumni in the different government, non-government and private organizations and companies, USeP receives various scholarships from congressmen, senators and private
              individuals and companies to help deserving USeP students and faculty. {'\n'} {'\n'}

              {'\t'} {'\t'} Now on its 37th year of service, USeP has all the reasons to be proud of its achievements in the areas of instruction, research, extension, production, and administration. To keep its reputation as a quality education service provider, USeP’s administration, faculty and students will continue to work to ensure that its distantly located but diversely-rich network of campuses continue to perform effectively and efficiently to serve the needs of Region XI and the country. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>
          </View>
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 40
  }
});


export default PresentImes;