import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class ClassSize extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Class Size</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Academic Calendar')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Schedule of Class')}               
              />
            </Button>
          </Right>
        </Header>
        
        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
            {'\t'} {'\t'} Generally, the class size for all regular undergraduate courses range from thirty
            (30) to fifty (50) students. Class size beyond 50 may be allowed in situations where there
            is shortage of faculty members and classrooms for as long as the quality of instruction is
            not sacrificed. {'\n'} {'\n'}

            {'\t'} {'\t'} A class size of less than thirty (30) but not lower than ten (10) is allowed for
            major/specialization projects provided that it is the only class for that subject offered in
            that semester. A class size of less than ten (10) will be considered a tutorial class and may
            still be offered if the faculty member who will handle the course is willing to teach the
            subject. {'\n'} {'\n'}

            {'\t'} {'\t'} For the regular course offerings of graduate programs, the minimum number of
            students required to officially commence a core or basic course and for an advance or
            major course is fifteen (15) and ten (10), respectively. The Dean may decide to dissolve
            the class or maintain it as a “special class” or “tutorial class” in the situation that the class
            size falls below the minimum number. If the class is dissolved, the Dean will
            immediately advise the concerned students to drop or enrol other courses. {'\n'} {'\n'}

            {'\t'} {'\t'} No class shall be divided into sections for either of the following causes: {'\n'} {'\n'} 

            {'\t'} {'\t'} a) To suit the personal preference of the individual faculty member withregard to time and place; and {'\n'} {'\n'} 

            {'\t'} {'\t'} b) To enable the faculty member to comply with the regulations governing teaching load. {'\n'} {'\n'} 
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

ClassSize.propTypes = {

};

export default ClassSize;