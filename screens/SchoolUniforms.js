import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class SchoolUniforms extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>School Uniforms</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('School Tuition and Miscellaneous Fees')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('School Identification Card')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              <Text style = {{fontWeight: 'bold'}}>
                Regular Uniforms {'\n'} {'\n'}              
              </Text>

              {'\t'} {'\t'} School uniforms identify the students as belonging to the USeP community thus it
              must be worn with respect and dignity. {'\n'} {'\n'}

              {'\t'} {'\t'} Regular uniform for men consists of white polo, white undershirt/sando, slacks or
              in considerable case, any dark pants. Uniform for women, consist of maroon chequered
              skirt (1 inch below the knee) and white blouse with a necktie. {'\n'} {'\n'}

              {'\t'} {'\t'} Only students wearing proper uniforms will be allowed to enter the university premises. {'\n'} {'\n'}

              {'\t'} {'\t'} All students are required to wear proper school uniforms except on Wednesdays and Summer Classes. {'\n'} {'\n'}

              {'\t'} {'\t'} Exemption of wearing the school uniforms are only given to those students with
              valid reasons (e.g pregnant, working students). Request for exemption for the said
              reasons is made at the Office of the Student Services. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Physical Education (PE) Uniforms {'\n'} {'\n'}              
              </Text>

              {'\t'} {'\t'} PE uniform for both men and women consists of maroon jogging pants with
              yellow vertical lines on the side with USeP print/patch and white shirt with the USeP
              logo print on the front left chest and “Physical Education” print at the back. Physical
              Education (P.E) uniforms should be worn only during PE classes. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                On–the-Job Training (OJT) Uniforms {'\n'} {'\n'}              
              </Text>

              {'\t'} {'\t'} All OJT uniforms must be approved by the Local Academic Council, Academic
              Council. The Office of Student Services will be informed regarding this matter. Students
              are allowed to enter the university premises wearing their complete OJT uniforms with
              either school ID or nameplate prominently displayed. {'\n'} {'\n'} {'\n'}
            </Text>

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic', textAlign: 'center'}}>
                Regular Uniforms for Male and Female Students {'\n'} {'\n'}              
              </Text>
            

            <Image style = {{alignSelf: 'center'}}
                source = {require('../images/boy.png')}
            />

            <Text>
              {'\n'}
            </Text>

            <Image style = {{alignSelf: 'center'}}
                source = {require('../images/girl.png')}
            />

            <Text>
              {'\n'} {'\n'} {'\n'}
            </Text>

            <Text style = {{fontWeight: 'bold', fontStyle: 'italic', textAlign: 'center'}}>
              PE Uniforms for Male and Female Students {'\n'} {'\n'}               
            </Text>

            <Image style = {{alignSelf: 'center'}}
                source = {require('../images/pe1.png')}
            />

            <Text>
              {'\n'}
            </Text>

            <Image style = {{alignSelf: 'center'}}
                source = {require('../images/pe2.png')}
            />

            <Text>
              {'\n'}
            </Text>

            <Image style = {{alignSelf: 'center'}}
                source = {require('../images/pe3.png')}
            />

            <Text>
              {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

SchoolUniforms.propTypes = {

};

export default SchoolUniforms;