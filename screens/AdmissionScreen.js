import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class AdmissionScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Admission</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Organizational Structure')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Registration')}               
              />
            </Button>
          </Right>
        </Header>
        
        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} The University of Southeastern Philippines welcomes all qualified students regardless of
              age, sex, religious beliefs, physical disabilities, nationality or political affiliations.
              Entrance requirements for each graduate and undergraduate program are prescribed by
              the individual colleges and approved by the University President.  {'\n'} {'\n'}

              {'\t'} {'\t'} It is necessary that a student is in good health, as certified by competent medical
              authorities recognized by the university, to be allowed to enroll. If the student is found to
              be medically unfit to study, the privilege of matriculation may be withdrawn upon
              recommendation of a competent authority. {'\n'} {'\n'}

              {'\t'} {'\t'} Qualified students with deficient admission requirements may be provisionally
              admitted provided they make up for all deficiencies within one year. {'\n'} {'\n'}

              {'\t'} {'\t'} Students transferring from other schools follow the admission process and
              requirements laid out for new students. Some exemptions or additions may be set by the
              Office of the University Registrar (OUR). {'\n'} {'\n'}

              {'\t'} {'\t'} For students coming from foreign countries, the substantial equivalence of
              courses completed with those prescribed by the University will be considered, if such
              courses were taken in an institution of recognized standing. Immigration and the
              Commission on Higher Education (CHED) requirements should likewise be complied. {'\n'} {'\n'}

              {'\t'} {'\t'} Every student shall, upon admission, sign the following pledge: {'\n'} {'\n'}
              
              <Text style = {{fontStyle: 'italic', justifyContent: 'flex-end'}}>
                As a student privileged to be admitted to the University of Southeastern
                Philippines, I hereby promise to abide by all the rules and regulations laid down
                by its competent authority. {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} Refusal to take this pledge shall be sufficient cause for denial of admission. {'\n'}

              {'\t'} {'\t'} The University may limit or close the admission of students depending on the
              availability of the faculty and facilities. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Admission Requirements {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} For Graduate School {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'}  - Entrance Exam Result, if applicable {'\n'}
              {'\t'} {'\t'} {'\t'}  - Transcript of Records {'\n'}
              {'\t'} {'\t'} {'\t'}  - Honorable Dismissal {'\n'}
              {'\t'} {'\t'} {'\t'}  - 2 copies of 2x2 ID pictures {'\n'}
              {'\t'} {'\t'} {'\t'}  - Personal Data Sheet {'\n'} {'\n'} {'\n'}

              {'\t'} {'\t'} For Undergraduate Students {'\n'} {'\n'}
              
              <Text style = {{fontStyle: 'italic'}}>
                {'\t'} {'\t'} {'\t'} New Students {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} {'\t'}  - Form 138 (High School Card) {'\n'}
              {'\t'} {'\t'} {'\t'}  - Birth Certificate (Authenticated by NSO) {'\n'}
              {'\t'} {'\t'} {'\t'}  - Entrance Exam Result (USePAT) {'\n'}
              {'\t'} {'\t'} {'\t'}  - Honorable Dismissal-Original (transferees only) {'\n'}
              {'\t'} {'\t'} {'\t'}  - Photocopy of Transcript of Records (transferees only) {'\n'}
              {'\t'} {'\t'} {'\t'}  - Medical Certificate (original copy) {'\n'}
              {'\t'} {'\t'} {'\t'}  - Student Personal Information Sheet {'\n'}
              {'\t'} {'\t'} {'\t'}  - Course Prospectus {'\n'}
              {'\t'} {'\t'} {'\t'}  - Good Moral Certificate (original copy) {'\n'}
              {'\t'} {'\t'} {'\t'}  - 2 pcs. 2x2 recent ID picture {'\n'}
              {'\t'} {'\t'} {'\t'}  - Brown Envelope- long {'\n'}
              {'\t'} {'\t'} {'\t'}  - Official Receipts of tuition and other fees {'\n'}
              {'\t'} {'\t'} {'\t'}  - Psychological Test Result {'\n'}
              {'\t'} {'\t'} {'\t'}  - Admission slip {'\n'} {'\n'} {'\n'}

              <Text style = {{fontStyle: 'italic'}}>
                {'\t'} {'\t'} {'\t'} Old Students {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} {'\t'}  - Clearance {'\n'}
              {'\t'} {'\t'} {'\t'}  - Official Receipts of tuition and other fees {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Returning Students {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} Students who were not enrolled during the preceding semester, excluding summer
              session, and did not obtain clearance from the university and want to return to study
              should secure and submit the following to the University Registrar: {'\n'} {'\n'}

              {'\t'} {'\t'} a) Written permission to enroll from the college/academic unit where they were
              last enrolled. {'\n'} {'\n'}
              
              {'\t'} {'\t'} b) Physical and medical examination results from the University Clinic or any
              government hospital. {'\n'} {'\n'}

              {'\t'} {'\t'} c) Psychological exam result {'\n'} {'\n'}

              {'\t'} {'\t'} Former students who secured clearance from the University must apply for readmission
              at the Office of the University Registrar. Students who have attended another
              institution after attending the University of Southeastern Philippines qualify on the same
              basis as transferees. {'\n'} {'\n'}

              {'\t'} {'\t'} The tuition and miscellaneous fees of readmitted students follow the rate enjoyed
              by the regular studentsbelonging to his/her year level. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Transferees {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} The following rules shall govern the admission of transfer students: {'\n'} {'\n'}

              {'\t'} {'\t'} a) A transfer student may be admitted provided that: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} - He/She must have obtained an average of “2”, 86% or, “B”, or better,
                                     in all the collegiate academic units he has earned outside; and {'\n'} {'\n'}
              {'\t'} {'\t'} {'\t'} - He will have to complete in this university no less than 50 percent of
                                     the units required for the course;{'\n'} {'\n'}

              {'\t'} {'\t'} b) The admission of transfer students shall be on probation basis until such time
              when he/she shall have validated or repeated the required courses outside the
              university in accordance with the rules stated in (d) below, which are required
              for the course; {'\n'} {'\n'}

              {'\t'} {'\t'} c) An admitted transfer student may not be allowed to enroll in the subject or
              subjects of which the prerequisite taken elsewhere have not been validated or
              repeated; {'\n'} {'\n'}

              {'\t'} {'\t'} d) An admitted transfer student must validate all the subjects he/she is offering
              for advanced credits at the rate of at least 15 units a semester within a period
              not exceeding four semesters from the date of his/her admission. Failure to
              comply with this requirement shall be a sufficient ground for the cancellation
              of his registration privileges; and {'\n'} {'\n'}

              {'\t'} {'\t'} e) Any or all of the above rules may be set aside in exceptional cases upon the
              recommendation of the Committee on Admission in the college or unit where
              admission of students is passed upon by this Committee or of the Dean concerned. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

AdmissionScreen.propTypes = {

};

export default AdmissionScreen;