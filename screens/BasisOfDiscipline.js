import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class BasisOfDiscipline extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Basis of Discipline</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Student Conduct')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Committee on Student Discipline')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} At all times, every student must observe/follow/abide by all the laws of the land,
              and all the policies and regulations adopted by the University/College. The investigation,            
              disposition and the corresponding sanction on student disciplinary cases shall follow the
              procedures set in his Handbook. {'\n'} {'\n'}

              {'\t'} {'\t'} Every student shall observe at all times the Pledge of Loyalty and Discipline to the institution. {'\n'} {'\n'}

              {'\t'} {'\t'} The maintenance of student conduct and discipline is anchored on the willful
              acceptance by the student of all policies, rules and regulations prescribed by the
              University/College as signified by the enrolment pledge and the guidance and counseling
              provided by the faculty who shall be exercising substitute authority. All school personnel
              and the duly elected officers of the Student Government are mandated to enforce and
              supervise overall compliance to this Handbook and the Code of the University in their
              respective areas of responsibility. {'\n'} {'\n'}

              {'\t'} {'\t'} For the purpose of implementing University/College policies, rules and
              regulations and the provision of the Code of the University, the president, deans,
              directors, members of the faculty and staff and duly elected officers of the Student
              Government are persons in authority. {'\n'} {'\n'}

              {'\t'} {'\t'} A student shall be subject to disciplinary action after due process for any of the following offenses: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Any form of cheating in examinations or any act of dishonesty during the period of enrolment in the University; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Creating and/or participating in disorders, tumult, breach of peace, or other serious disturbances in the University premises; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Connecting electrical wires without permission from proper authorities; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Climbing or jumping over the boundary fence of the University; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Acts of bribery to gain favor in violation of the Standards of Instructions; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Littering or scattering of garbage in public places; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Insulting, uttering derogatory remarks or flagrant indecent language against the teachers and persons in authority, or students of the University inside the campus; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Intentionally making a false statement of any material fact, or practicing or
              attempting to practice any deception or fraud in connection with his/her admission to or registration in, or graduation from the University; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Preventing or threatening students, faculty and administrators from discharging their duties or from attending their classes or entering school premises; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Tampering with and/or lending I.D. card to students or outsiders; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Vandalism or destruction of public property, such as destruction of building parts/fixtures/walls, tearing of pages of library books, magazines; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Fighting or influencing physical injuries as a way to settle disputes; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Illegal posting of posters and buntings; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Failure to wear the prescribed uniform required by the University; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Smoking while in the campus premises; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Drinking alcoholic beverages, or exhibiting drunken behavior, within the University premises; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Gambling within the University premises; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Any form of public immorality (lascivious, malicious acts, etc.) on campus or during university function; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Forging of signatures and falsification of documents; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Robbery (attempted, frustrated and consummated); {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Theft (attempted, frustrated and consummated); {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Unauthorized possession of firearms, explosives such as grenades, and ammunitions in one’s person or custody, and/or other deadly weapons;; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Ingestion, use, possession and/or peddling of dangerous regulated drugs or paraphernalia; {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • Malversation of student funds; and, {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} • All other disciplinary cases not mentioned therein. {'\n'} {'\n'} {'\n'}
            </Text>

            <Text style = {{fontWeight: 'bold', textAlign: 'center'}}>
              Sanctions {'\n'} {'\n'}               
            </Text>

            <Text>
              1. Disciplinary action may take any of the following forms: {'\n'} {'\n'}

              {'\t'} {'\t'} a. Warning {'\n'} {'\n'}

              {'\t'} {'\t'} b. Reprimand {'\n'} {'\n'}

              {'\t'} {'\t'} c. Demand for apology by the student concerned {'\n'} {'\n'}

              {'\t'} {'\t'} d. Payment of actual damage inflected {'\n'} {'\n'}

              {'\t'} {'\t'} e. Automatic failure of the subject or test {'\n'} {'\n'}

              {'\t'} {'\t'} f. Exclusion from attending recognized clubs {'\n'} {'\n'}

              {'\t'} {'\t'} g. Disqualification from holding any position in any organization either by election or appointment {'\n'} {'\n'}

              {'\t'} {'\t'} h. Withholding of graduation and other privileges {'\n'} {'\n'}

              {'\t'} {'\t'} i. Cancellation of scholarship {'\n'} {'\n'}

              {'\t'} {'\t'} j. Suspension {'\n'} {'\n'}

              {'\t'} {'\t'} k. Dismissal {'\n'} {'\n'}

              {'\t'} {'\t'} l. Expulsion {'\n'} {'\n'} {'\n'}

              2. The gravity of the offense committed and the circumstances attending its commission
              determines the nature of the disciplinary action or penalty to be imposed. {'\n'} {'\n'}

              3. No student is disciplined through suspension, dismissal, or reduction of his/her
              privileges until an investigation is held wherein the respondent is given the
              opportunity to be heard. {'\n'} {'\n'}

              4. Any disciplinary action taken against a student is immediately reported to his or her
              parents or guardians. {'\n'} {'\n'}

              5. A student not enrolled in the University who refuses to submit to the jurisdiction of
              the University at the time a charge against him is filed or has pending litigation
              prejudices his/her future enrolment in any unit of the University. {'\n'} {'\n'}

              6. Where the suspension is for one semester or more, the student should move out of the
              University jurisdiction within 72 hours after the suspension order takes effect. Any
              student whose suspension covers the final examination period will have to miss the
              final examination. In all cases of suspension, a written promise of future exemplary
              conduct by the student, which is countersigned by his parents or guardians, is
              required as a condition for readmission. {'\n'} {'\n'}

              7. When a penalty of expulsion is meted, the student cannot enroll in any course in the
              University. He/She cannot get his Transfer Credentials within one semester. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

BasisOfDiscipline.propTypes = {

};

export default BasisOfDiscipline;