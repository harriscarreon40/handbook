import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class UsepVMGC extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
            <Left>
              <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
                this.props.navigation.openDrawer()} />
            </Left>
            <Body>
              <Text>USeP VMGC</Text>
            </Body>
            <Right>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                  onPress={() => navigate('Preface')}               
                />
              </Button>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                  onPress={() => navigate('University History')}               
                />
              </Button>
            </Right>
        </Header>

        <ScrollView>
          <View style={[styles.container]}>                 
            <Text style = {{fontWeight: 'bold'}}>
              VISION {'\n'} {'\n'}
            </Text>

            <Text>
              {"\t"} {"\t"} To be a premier university in the ASEAN Region, USeP shall be a center of excellence and development, responsive and adaptive to fast-changing environments. USeP shall also be known as the leading university in the country that fosters innovation and applies knowledge to create value towards social, economic and technological developments. 
              {'\n'}
              {'\n'}
              {'\n'}
            </Text>

            <Text style = {{fontWeight: 'bold'}}>
              MISSION {'\n'} {'\n'}
            </Text>

            <Text>
              {"\t"} {"\t"} USeP shall produce world-class graduates and relevant research and extension through quality education and sustainable resource management. {'\n'} {'\n'}
              
              Particularly, USeP is committed to:
              {'\n'}
            </Text>

            <Text style = {[styles.content]}>
              • Provide quality education for students to grow in knowledge, promote their wellrounded development, and make them globally competitive in the world of work;
            {'\n'}
            </Text>

            <Text style = {[styles.content]}>  
              • Engage in high impact research, not only for knowledge's sake, but also for its practical benefits to society; and, 
            {'\n'}
            </Text>

            <Text style = {[styles.content]}>
              • Promote entrepreneurship and industry collaboration.
            {'\n'} {'\n'} {'\n'}
            </Text>

            <Text style = {{fontWeight: 'bold'}}>
              GOALS {'\n'} {'\n'}
            </Text>

            <Text>
              {"\t"} {"\t"} Aligned with the university’s vision and mission are specific goals for Key Result Areas (KRA) on Instruction; Research, Development, and Extension; and Resource Management: {'\n'}
            </Text>

            <Text>
              <Text style = {{fontWeight: 'bold'}}>
                KRA 1: Instruction - {' '}
              </Text> 
              Produce globally competitive and morally upright graduates. {'\n'}
            </Text>

            <Text>
              <Text style = {{fontWeight: 'bold'}}>
                KRA 2: Research Development and Extension (RDE) - {' '}
              </Text> 
              Develop a strong RDE culture with competent human resource and responsive and relevant researches that are adapted and utilized for development. {'\n'}
            </Text>

            <Text>
              <Text style = {{fontWeight: 'bold'}}>
                KRA 3: Resource Management - {' '}
              </Text>
              Effective and efficient generation, allocation and utilization of resource
              {'\n'}
              {'\n'}
              {'\n'}
            </Text>

            <Text style = {{fontWeight: 'bold'}}>
              CORE VALUES {'\n'} {'\n'}
            </Text>

            <Text>
              USeP is a community of scholars that values: {'\n'} {'\n'}
            </Text>

            <Text style = {{textAlign: 'left'}}>
              <Text style = {{fontWeight: 'bold'}}>
                U
              </Text>
              nity
            </Text>

            <Text style = {{textAlign: 'left'}}>
              <Text style = {{fontWeight: 'bold'}}>
                S
              </Text>
              tewardship
            </Text>

            <Text style = {{textAlign: 'left'}}>
              <Text style = {{fontWeight: 'bold'}}>
                E
              </Text>
              xcellence
            </Text>

            <Text style = {{textAlign: 'left'}}>
              <Text style = {{fontWeight: 'bold'}}>
                P
              </Text>
              reofessionalism {'\n'} {'\n'}
            </Text>

            <Image style = {{alignSelf: 'center'}}
                source = {require('../images/usep.jpg')}
              />
            
            <Text>
               {'\n'} {'\n'}
                As a learning organization, we shall demonstrate {' '}
               <Text style = {{fontWeight: 'bold'}}>
                PROFESSIONALISM
              </Text> in all our dealings, promote {' '}
              <Text style = {{fontWeight: 'bold'}}>
                UNITY
              </Text> among us and our stakeholders harness {' '}
              <Text style = {{fontWeight: 'bold'}}>
                STEWARDSHIP
              </Text> in managing our resources in order to exemplify {' '}
              <Text style = {{fontWeight: 'bold'}}>
                EXCELLENCE
              </Text> in Instruction, Research, Extension, Production and Development. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 40
  },
  content: {
    marginLeft: 20
  }
});

UsepVMGC.propTypes = {

};

export default UsepVMGC;