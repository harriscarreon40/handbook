import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class UniversityHistory extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
            <Left>
              <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
                this.props.navigation.openDrawer()} />
            </Left>
            <Body>
              <Text>University History</Text>
            </Body>
            <Right>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                  onPress={() => navigate('USeP VMGC')}               
                />
              </Button>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                  onPress={() => navigate('Present Times')}               
                />
              </Button>
            </Right>
        </Header>

        <ScrollView>
          <View style={[styles.container]} >
            <Text>
              {'\t'} {'\t'} The University of Southeastern Philippines, the only state university in Region XI, was created on December 15, 1978 through the passage of Batas Pambansa Bilang 12. {'\n'} {'\n'}

              {'\t'} {'\t'} Batas Pambansa Bilang 12 promulgated the integration of four state institutions to create the University of Southeastern Philippines (USeP). USeP became operational in 1979 after the Mindanao State University-Davao Branch (MSUDB), the University of the Philippines Master of Management Program in Davao (UPMMPD), the Davao School of Arts and Trades (DSAT) and the Davao National Regional Agricultural School (DNRAS) were merged.Over the years since then, the university expanded from its first site to five campuses namely: the Davao City main campus in Obrero with an area of 6.5 hectares; the Mintal campus also in Davao City that has an area of 2.8 hectares; the Tagum campus in Tagum City with a 77-hectare land area; the Mabini campus which lies in a 109-hectare land area in Mampising, Mabini, Compostela Valley Province; and the latest addition, the Bislig campus in Bislig City, Surigao del Sur with an area of 9.7 hectares. {'\n'} {'\n'}

              {'\t'} {'\t'} In 1993, through a Board of Regents (BOR) resolution the External Studies Programs were established. These were: USeP-Hinatuan Ext. Program in Surigaodel Sur; USeP-Baganga External Studies Program in Davao Oriental; USEP-Kapalong College of Agricultural Technology and Entrepreneurship and USeP-Pantukan External Studies Program. {'\n'} {'\n'}
              
              {'\t'} {'\t'} In order to serve a greater population in the region, the Evening Program was implemented through BOR resolution No. 2732 which accommodates students who could not attend the regular day programs. In the same vein, the university began offering Summer Programs. The university has also recognized the need to expand its course offerings. {'\n'} {'\n'}

              {'\t'} {'\t'} To better serve its stakeholders, the university through its Board of Regents continues to streamline its administrative and organizational structure to be able to 4 respond to the region’s changing needs and enable USeP to engage in more relevant research and extension work. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 40
  }
});

UniversityHistory.propTypes = {

};

export default UniversityHistory;