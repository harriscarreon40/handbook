import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class OUR extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>OUR</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('ULRC')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('OSAS')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              <Text style = {{fontWeight: 'bold'}}>
                Enrollment {' '}              
              </Text>
              (New, Transferees, and Old Students) {'\n'} {'\n'}

              {'\t'} {'\t'} Schedule of Availability of Service: Enrollment period {'\n'} {'\n'}

              {'\t'} {'\t'} Who May Avail of the Service: New, old and transfer students {'\n'} {'\n'}

              {'\t'} {'\t'} Requirements: Please refer to the “Admission” on the academic policies for the requirements. {'\n'} {'\n'}

              {'\t'} {'\t'} Duration: 5-10 minutes {'\n'} {'\n'}

              {'\t'} {'\t'} How to avail of the service: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 1. Submit all the requirements at the designated counters. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 2. Affix signature on the COR (Registrar’s & Student’s Copy). {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Application for New Copy of Diploma {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Schedule of Availability of Service: Monday to Saturday, 8:00 am - 5:00 pm {'\n'} {'\n'}

              {'\t'} {'\t'} Who May Avail of the Service: Alumni Students {'\n'} {'\n'}

              {'\t'} {'\t'} Requirements: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 1. Request letter addressed to: {'\n'} {'\n'}
            </Text>

            <Image style = {[styles.fit]}
              source = {require('../images/req.png')}
            />

            <Text>
              {'\t'} {'\t'} {'\t'} {'\t'} 2. Affidavit of Loss in case of lost diploma {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 3. Proof of destroyed or damaged diploma {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 4. Official Receipt (OR) of payment of fees {'\n'} {'\n'}
            </Text>

            <Text style = {{textAlign: 'center'}}>
              Php 134.00 – Higher Education courses {'\n'} {'\n'}
              Php 200.00 – Advanced Studies{'\n'} {'\n'} {'\n'}            
            </Text>
            
            <Text>
            {'\t'} {'\t'} Duration: 5-10 working days (depends on the availability of the signatories) {'\n'} {'\n'}

            {'\t'} {'\t'} How to avail of the service: {'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} 1. Present all the requirements at the Receiving Counter. {'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} 2. Proceed to the Cashier and secure Official Receipt (OR). {'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} 3. Submit the OR together with all the required documents at the Receiving Counter. {'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} 4. Present claim slip on the scheduled date of release at the Releasing Counter {'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} 5. Sign in the logbook and indicate type of document/s received. {'\n'} {'\n'} {'\n'}

            <Text style = {{fontWeight: 'bold'}}>
              Authentication of Documents {'\n'} {'\n'}               
            </Text>
              {'\t'} {'\t'} Schedule of Availability of Service: Monday to Saturday, 8:00 am - 5:00 pm Who May Avail of the Service: Students {'\n'} {'\n'}

              {'\t'} {'\t'} Requirements: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 1. Original Copy of documents {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 2. Photocopies of documents {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 3. Official Receipt (OR) of payment of fees {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 4. Documentary Stamps ( 1 pc. documentary stamp per document) {'\n'} {'\n'} {'\n'}

              {'\t'} {'\t'} Duration: 15 minutes {'\n'} {'\n'}

              {'\t'} {'\t'} How to avail of the service: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 1. Present all the original and photocopies of documents at the Receiving Counter. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 2. Proceed to the Cashier and secure Official Receipt (OR). {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 3. Submit the OR together with all the required documents at the Receiving Counter. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 4. Wait for name to be called at the Releasing Counter. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 5. Sign in the logbook and indicate type of document/s received. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

OUR.propTypes = {

};

export default OUR;