import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class AcademicCalendar extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Academic Calendar</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Classification of Students')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Class Size')}               
              />
            </Button>
          </Right>
        </Header>
        
        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
            {'\t'} {'\t'}The general framework and details of the academic calendar, which conforms to
            Commission on Higher Education (CHED) rules, is prepared by the Office of the
            University Registrar, and reviewed by the Deans’ Council for approval by the University
            President. {'\n'} {'\n'}

            {'\t'} {'\t'} Except for approved special programs, all academic units of the University
            operate under the semestral system with a summer term wherein classes are generally
            scheduled from Monday to Saturday. Each semester shall consist of at least eighteen (18)
            weeks of classes. The summer term, on the other hand, runs for six (6) weeks. {'\n'} {'\n'}

            {'\t'} {'\t'} Class work in every course in the summer session is equivalent to class work in the same course in any semester. {'\n'} {'\n'} 
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

AcademicCalendar.propTypes = {

};

export default AcademicCalendar;