import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class GradingSystem extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Grading System</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Examinations')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Recognition of High Scholastic Achievement')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} At the beginning of the semester, students are informed about the criteria for
              grading in accordance with certain standards established by the College/Academic
              Unit/Department. Some items that can be considered for grading are: periodic
              examinations, term papers, projects, oral report/presentation, and class participation. The
              teacher determines the weight allocation for each item.  {'\n'} {'\n'}

              {'\t'} {'\t'} Only duly registered students are to be given ratings and this can be double
              checked through the official class list. The rating system shall be uniform with an
              interval of 0.25 where 1.0 is the highest passing grade and 3.0 is the lowest passing grade.
              A rating of 5.0 is failure.{'\n'} {'\n'}

              {'\t'} {'\t'} The detailed rating system is as follows: {'\n'} {'\n'}              
            </Text>

            <Image style = {{flex: 1, alignSelf: 'center'}}
              source = {require('../images/grade.png')}
            />

            <Text style = {{fontWeight: 'bold'}}>
              {'\n'} {'\n'} {'\n'} Compliance of Incomplete Grades (INC) {'\n'} {'\n'}
            </Text>

            <Text>
              {'\t'} {'\t'} An INC grade is given to a student whose class standing throughout the semester
              or term is passing but fails to take the final examination or fails to complete other course
              requirements due to illness or valid reasons. Thereupon, a special examination or
              completion requirements shall be given to him/her by the faculty concerned or by the
              Department Chair upon payment of a required fee per course. {'\n'} {'\n'} 

              {'\t'} {'\t'} In case the class standing is not passing, and the student fails to take the final
              examination for any reason, a grade of "5" is given. {'\n'} {'\n'} 

              {'\t'} {'\t'} INC is also given for a work that is of passing quality but some part of which for
              good reason is unfinished. {'\n'} {'\n'}

              {'\t'} {'\t'} The deficiency indicated by the grade of "INC" must be removed within the
              prescribed period of one year; otherwise it will be converted to a grade of 5.0 by the
              faculty concerned. In case of unavailability of the concerned faculty, the department
              chair/program head is authorized to take charge in the completion of the INC. {'\n'} {'\n'}

              {'\t'} {'\t'} For scholarship grantees, such deficiency should be completed or removed within
              a period before the opening of classes of the following semester. {'\n'} {'\n'}

              {'\t'} {'\t'} Students not in residence shall pay the registration fee on top of the removal fee in
              order to be entitled to remove his/her INC grade{'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Change of Grades {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} A student who has received a passing grade in a given course is not allowed reexamination
              for the purpose of improving his grades. Notwithstanding the foregoing
              provision and to avoid any injustice, the grade on a final examination paper may be
              revised by a committee constituted by the Dean of the College if it should clearly
              appear, on the basis of the quality of the scholastic record of the student, that such grade
              is the result of an erroneous appreciation of the answers or of an arbitrary or careless
              decision by the faculty member concerned. Should the change of the grade on said paper
              affect the final grade of the student, the committee may request authority from the
              College Council to make the necessary change in the final grade. The request for
              reconsideration shall be made within 30 days after the receipt of the final grade by the
              student concerned. {'\n'} {'\n'}

              {'\t'} {'\t'} In no case shall a grade be changed beyond one (1) year after initial filing; nor
              shall any change operate to the prejudice of the student.{'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Submission of Grades {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Non-graduating students can expect to receive their grades on the following schedule: {'\n'} {'\n'}

              {'\t'} {'\t'} • 1st semester and summer - Five (5) days after the last day of final examination. {'\n'} {'\n'}

              {'\t'} {'\t'} • 2nd semester - Ten (10) days after the last day of the final examination. {'\n'} {'\n'}

              {'\t'} {'\t'} For graduating students, however, they can receive their grades ahead of the
              schedule for the non-graduating students. The dates for submission of grading sheets by
              faculty members are posted on the University calendar.{'\n'} {'\n'}

              {'\t'} {'\t'} A clear copy of class records and the report of ratings is available at the
              College/Department for reference in cases of inquiries regarding computations and
              incomplete grades.{'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Release of Grades {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} A student copy of semestral grade can be obtained from the
              college/department/school for free upon request or print a copy via the university
              website’s online grade copies. Furthermore, students can get a certificate of grades with
              documentary stamp at the registrar’s office upon request. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}


            </Text>

          </View>

        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

GradingSystem.propTypes = {

};

export default GradingSystem;