import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class PolicyScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Policy on Student Records</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Commencement and Baccalaureate Services')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Scholastic Delinquency')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} The University maintains various records of students to document their academic
              progress as well as to record their interaction with University staff and officials.
              Students’ records are generally considered confidential except for the directory of
              currently registered students. It is open to the public. The directory provides information            
              of each student’s name, I.D. number, college, program/course, classification and college
              address. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Transcript of Records {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Student records are confidential and information is released only at the request of
              the student or of appropriate institutions. “Partial” transcripts are not issued. Official
              transcript of records obtained from other institutions and submitted to the University for
              admission and/or transfer of credit become part of the student’s permanent record and are
              issued as true copies with the USeP transcript. {'\n'} {'\n'}

              {'\t'} {'\t'} Application for transcript of records is filed at the OUR upon presentation of the
              student clearance. A certain fee for transcript preparation will be charged to the
              concerned party. Graduates are encouraged to request for their transcripts as early as
              possible to avoid unnecessary delay. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Withholding of Records {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} When a student has pending financial obligations to the University, or when
              he/she has been charged with an official disciplinary action, the appropriate university
              official may request that the student’s record, e.g., transcripts, diploma, registration
              forms, be withheld. Departments and offices for example, should submit before the end
              of every semester/term the names of students with financial accountabilities to the OUR
              so that action may be taken. If the student has already settled his/her obligations, the
              OUR must receive written authorization from the official who originally requested the
              action, indicating that the student has already settled the obligation. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}
                
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

PolicyScreen.propTypes = {

};

export default PolicyScreen;