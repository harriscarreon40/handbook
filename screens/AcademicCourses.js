import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class AcademicCourses extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Academic Courses</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Registration')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Withdrawal From The University')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              <Text style = {{fontWeight: 'bold'}}>
                Rule on Pre-requisite Subjects {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} Courses considered by the University Council and approved by the Board of
              Regents as pre-requisites to other courses should be strictly enforced. Pre-requisites shall
              be taken and passed before enrolling in requisite subjects. However, in meritorious cases,
              like graduating students who are in their last semester of residence, simultaneous
              enrollment of the pre-requisites and requisite subjects are allowed. However, if they fail
              in the pre-requisite subjects, the grade of the requisite subjects shall also be invalidated. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Changing of Courses {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} Changing of Academic Program shall be made only for valid reasons. Approval
              from the Deans and faculty concerned must be sought after which the University
              Registrar shall be properly notified immediately. No transfers shall be allowed after ten
              (10) days from the start of classes. {'\n'} {'\n'}

              {'\t'} {'\t'} If a student withdraws after 75% of the total number of hours prescribed for the
              course has already elapsed, his/her teacher may give him a  grade of 5.0 if his class
              standing up to the time of his withdrawal was below 3.0. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Dropping of Courses {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} Students are allowed to drop the courses they enrolled with the consent of their
              professors and duly approved by the Department Chair and the Dean of the College. They
              are subject to the following conditions: {'\n'} {'\n'}

              {'\t'} {'\t'} a) A student who wishes to drop a subject accomplishes the prescribed form of the University; {'\n'} {'\n'}
              
              {'\t'} {'\t'} b) A student who drops a subject on or before the mid-term period will have his/her records marked with “AW” ( Authorized Withdrawal); {'\n'} {'\n'}

              {'\t'} {'\t'} c) A student who drops a subject after the mid-term shall earn a failing grade or“5.0”; and {'\n'} {'\n'}

              {'\t'} {'\t'} d) A student who drops a subject without official approval shall have his/her records marked “UW” (Unauthorized Withdrawal) and automatically gets a grade of 5.0.{'\n'}  {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Substitution of Courses {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} Students may be allowed to substitute courses based on any of the following conditions: {'\n'} {'\n'}

              {'\t'} {'\t'} a) When a student is pursuing a curriculum that has been superseded by a new
              one and the substitution tends to bring the old curriculum in line with the new; {'\n'} {'\n'}

              {'\t'} {'\t'} b) When there is conflict of schedule between two required courses during the last semester of his/her study; {'\n'} {'\n'}

              {'\t'} {'\t'} c) When the required course is not offered during the last semester and the student is scheduled to graduate on the current semester; or {'\n'} {'\n'}

              {'\t'} {'\t'} d) When the student is deficient of a course and/or student has superior competence in the program/discipline desired. 
              {'\n'} {'\n'}

              Every petition for substitution requires: {'\n'} {'\n'}

              {'\t'} {'\t'} a) That the involved subjects are offered in the same department, if possible; if not, the two subjects must be allied to each other; {'\n'} {'\n'}

              {'\t'} {'\t'} b) That the number of units of the subject intended to substitute the required subject is equal or greater than number of units of the latter.{'\n'}  {'\n'} {'\n'}

              {'\t'} {'\t'} All petitions for substitution must be submitted to the Office of the Dean
              concerned for his/her action before the end of enrolment period. Any approved petition,
              which should strictly conform to the provisions for substitution of subjects, shall be
              considered for the ensuing or following semester. In case the Dean’s action does not
              conform to the recommendation of the adviser and the head of the concerned department,
              the student may appeal to the Vice President for Academic Affairs whose decision shall
              be final. {'\n'} {'\n'}

              {'\t'} {'\t'} Students will not be allowed to substitute any subject prescribed in the curriculum if
              they obtained a failing grade for it. An exception to the case would be when the subject is
              no longer offered. They may be allowed to do so provided that in the opinion of the
              department offering the prescribed subject the proposed substitution covers substantially
              the same subject matter as the required subject. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

AcademicCourses.propTypes = {

};

export default AcademicCourses;