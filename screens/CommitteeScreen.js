import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class CommitteeScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Committee on Student Discipline</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Basis of Discipline')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('UGTO')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} There shall be a Committee on Student Discipline composed of a Chairman, who
              shall be a member of the bar or with some legal background, two (2) members to be
              appointed for a period of one (1) year from among the Faculty of the University and two
              (2) students who shall be chosen by the respondent in a raffle from among the pool of
              Student Government officers in coordination with the Director of Student Services. {'\n'} {'\n'}

              {'\t'} {'\t'} The committee shall be under the general supervision of the Director of Student Services. {'\n'} {'\n'}

              {'\t'} {'\t'} Colleges/Units and Student Governments in the University shall set up a Subcommittee
              on Student Discipline to attend to cases within their jurisdiction. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Jurisdiction {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} All cases involving discipline of students shall be subject to the jurisdiction of the
              Committee on Student Discipline, except in cases, which shall fall under the jurisdiction
              of appropriate college/unit or sub-committee. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Procedures of Disciplinary Actions {'\n'} {'\n'}               
              </Text>

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Filing of Charges {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Disciplinary proceedings will be instituted as determined by the appropriate
              authority upon the filing of a written charge which specifies the acts or commissions
              constituting the misconduct. The written charges have to be subscribed to by the
              student/complainant including his/her parents or guardian. In the absence of a written
              charge, disciplinary proceedings may also be instituted upon submission of an official
              report of any violation of existing rules and regulations committed by a
              student/respondent. The said charges or report is filed with the Office of the Student
              Services (OSS) where an entry will be made in an official entry book kept for the purpose
              with the following details: the student/person charged; the complaint(s); witnesses, if any;
              the date of filing; and the substances of the charge. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Preliminary Inquiry {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Upon receipt of the complaint or report, the Director of the OSS will determine
              whether such complaint or report is sufficient to warrant formal investigations. The
              Director then will give a copy of the complaint or report to each student/respondent, and
              his/her parent/guardian. The respondents in turn will be required to answer the charges in
              writing. Notice to the respondent(s) during the preliminary investigation may be waived.
              In cases where the complaint or report is found sufficient, formal charges will be filed
              with the appropriate body. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Answer {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Each student/respondent is required to answer in writing, three (3) school days
              after they have received notice of the charge(s). A formal investigation will then be held
              on notice as provided below. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Notice of Hearing {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} All parties concerned will be notified of the time and date set for the hearing at
              least two (2) school days after the receipt of the notice. Notice to counsel of record or
              duly authorized representative of a party will be considered sufficient notice to such party
              for the purpose of this article. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Hearing {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} A hearing will begin not later than one (1) week after the respondent’s answer is
              received or after expiration of the period with which the student/respondent is given to
              answer. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Duration of Hearing {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} No hearing of any case shall last beyond two calendar months. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Failure to Appear at Hearing {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} The non-appearance of either the complainant or respondent at the designated
              place for the initial hearing after due notice - with or without sufficient justifications - will be noted and the hearing will proceed without prejudice to the party’s right of appearance in subsequent hearings. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Postponement {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Either party may apply for hearing postponement and may be granted for good
              cause for a period provided that the ends of justice and the rights of parties to a speedy
              hearing are respected. Each party to the litigation is allowed only a maximum of three
              postponements. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Sub-Committee Report {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} The College/Unit Investigating Sub-Committee shall forward the complete record
              of the case with its report and recommendations to the Dean/Unit Head concerned within
              fifteen (15) school days after the hearing is terminated. The report signed by a majority of
              the Members of the Committee shall state the findings or fact, conclusions and
              recommendations of the regulations of which the decision is based. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Decision by the Dean/Unit Head {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} The Dean/Unit Head shall transmit the report and the decision to the President of
              the University within ten (10) school days after receipt of the committee report. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Decision by the Committee of Student Discipline {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} The Chairman of the committee will decide each case within fifteen (15) school
              days after the final submission of the written decision and signed statement of the
              findings of the fact, conclusions and recommendations by the Committee and its
              Members. The report shall contain, in a brief statement, the findings of fact, conclusions
              and recommendations of the regulations from which the decision is based. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Finality of Decision {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} The decision of the Committee on Student Discipline or the Dean – i.e.
              sanctions other than expulsion, permanent disqualification from enrollment, or
              suspension for more than thirty (30) calendar school days - shall become final and
              executory after fifteen (15) school days it was received unless a Motion for              
              Reconsideration of the same is filed. In which case, the decision shall be final fifteen (15)
              school days after receipt of the denial of the Motion for Reconsideration. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Appeal to the President {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} In all cases in which a final decision is rendered by the Sub-Committee or the
              Committee on Student Discipline, the student/respondent may file an appeal with the
              Office of the President (OP) within ten (10) school days after receipt of the decision. The
              OP in turn will decide within ten (10) school days from receipt of the appeal. In cases
              concerning expulsion and suspension of one semester or more, the President shall consult
              the University Council. The President’s decision in these cases may be appealed to the
              Board of Regents within ten (10) school days after student/respondent receives a copy of
              such decision. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Decisions by the Board of Regents {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} The Board of Regents shall review appeals made against decisions of the
              President of the University that involve sanctions such as expulsion, suspension of one
              semester or more, or any penalty of equivalent severity and shall render final judgment
              thereof. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Rights of the Respondents {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Every student/respondent shall enjoy the following rights: {'\n'} {'\n'}

              {'\t'} {'\t'} a) Not to be subjected to any discipline/penalty before the requirements of due
              process are fully completed and complied with; {'\n'} {'\n'}

              {'\t'} {'\t'} b) Not to be convicted unless there is evidence, of which the burden of proof
              being on the person filing the charge; {'\n'} {'\n'}

              {'\t'} {'\t'} c) Not to be convicted without any basis of evidence introduced at the
              proceedings or of which the student/respondent has been properly apprised
              and given the opportunity to rebut the same; {'\n'} {'\n'}

              {'\t'} {'\t'} d) To enjoy - pending final decision and the charge - all his/her rights and
              privileges as a student, subject to the power of the preventive suspension of              
              the President for not more than fifteen (15) school days where suspension is
              necessary to maintain the security of the institution and; {'\n'} {'\n'}

              {'\t'} {'\t'} e) To defend himself/herself personally or by Counsel, or by a representative of
              his/her own choice. If the respondent desires but is unable to secure the
              services of a Counsel, he/she can request the Investigating Committee on
              Student Discipline or the Investigating Committee to designate a Counsel for
              him/her among the faculty members and staff of the institution at least two (2)
              school days before the hearing. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Summary Investigation by Dean {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Notwithstanding provisions of the foregoing articles, the Dean may proceed
              summarily against a student of his/her college for any of these acts: {'\n'} {'\n'}

              {'\t'} {'\t'} a) Violation of Rules and Regulations issued by the Dean of the College/unit; and {'\n'} {'\n'}

              {'\t'} {'\t'} b) Misconduct committed in the presence of a faculty member or of any official
              of the University within the classroom premises of the college/units or in the
              course of an official function sponsored by the college/unit. {'\n'} {'\n'}

              {'\t'} {'\t'} The respondent will be called to appear before the Dean of the College, be
              informed of the charges against him/her and be given the opportunity to present his/her
              side. {'\n'} {'\n'}

              {'\t'} {'\t'} Every decision made will be in issued in writing stating the facts of the case and
              the basis of the penalty imposed. Such decision will be final and executed immediately
              after the written order is issued. The penalty of suspension, if imposed, will not exceed
              thirty (30) school days. The Office of the Student Services will be provided with a copy
              of the decision. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Effectivity {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Decision(s) shall take effect as stipulated in these rules. Final decisions of
              suspensions or dismissal that are to be rendered within thirty (30) days prior to any final
              examination will take effect on the semester following the semester/summer in which
              such decision was rendered. However, in the case that the respondent is a graduating
              student, the penalty shall take effect immediately. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Records {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} All proceedings before any Sub-Committee or Committee on Student Discipline
              will be recorded in writing. Original records pertaining to student discipline shall be
              under the custody of the Director of the Office of Student Services. Such records are
              confidential and no person shall have access to these documents either for inspection or
              copying unless he/she is officially involved in the case. Any official or employee of the
              University who shall violate the confidential nature of such records shall be subject to
              disciplinary action. {'\n'} {'\n'}

              {'\t'} {'\t'} Note: Offenses and Penalties for Non-Faculty and Non-Students and where the
              provisions of this Handbook and the Code of the University cannot be applied, the case
              shall be brought to the appropriate Court of Justice. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Fact-Finding Committee {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} When necessary, the University President may create a Fact-Finding Committee
              to investigate cases/complaints affecting students. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

CommitteeScreen.propTypes = {

};

export default CommitteeScreen;