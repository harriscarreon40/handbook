import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class ScheduleOfClass extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Schedule of Class</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Class Size')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Special Classes')}               
              />
            </Button>
          </Right>
        </Header>
        
        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
            {'\t'} {'\t'}The schedule of classes is prepared by the Deans not later than one (1) month
            before the start of the semester or term and furnishes a copy to the Office of the
            University Registrar and the Office of the Vice President for Academic Affairs. Classes
            officially start at 7:00 A.M. and end at 10:00 P.M. {'\n'} {'\n'}

            {'\t'} {'\t'} Classes for every course is scheduled either twice (2) or thrice (3) a week on a
            Tuesday-Thursday or a Monday-Wednesday-Friday arrangements, respectively. Once
            (1) a week schedule for lecture classes may be allowed, under justifiable circumstances,
            provided there is prior approval from the Office of the Vice President for Academic
            Affairs. {'\n'} {'\n'}

            {'\t'} {'\t'}Laboratory classes however should be conducted once or for no more than two (2) sessions per week. {'\n'}  {'\n'} {'\n'}

            <Text style = {{fontWeight: 'bold'}}>
              Change of Class Schedule {'\n'} {'\n'}
            </Text>

            {'\t'} {'\t'} Classes should be held according to the official schedule and in the designated
            classrooms. Changes in schedule and location may be permitted with the approval of the
            College Dean. {'\n'}  {'\n'} {'\n'} 

            <Text style = {{fontWeight: 'bold'}}>
              First Day of Classes {'\n'} {'\n'}
            </Text>

            {'\t'} {'\t'} During the first day of classes, faculty members will check the registration
            certificates of their students. Students who are not officially enrolled are not allowed to
            attend his/her class, and will be advised to proceed to the Registrar’s Office. {'\n'} {'\n'} 

            {'\t'} {'\t'} A student is considered officially enrolled only after the University/Campus
            Registrar has duly certified his/her enrolment on the registration certificate. His/her name
            is included in the class list, issued by the College/Academic Unit. The class list contains
            the names of the students, regular or irregular, who are officially enrolled in a course. {'\n'} {'\n'} 

            {'\t'} {'\t'} On the first day of classes, students are oriented by their teachers about the Vision
            and Mission of the University. Likewise, course objectives, contents, requirements, and
            the bases for evaluating student’s performance are explained to them. The students
            should be provided with a copy of the course syllabus/outline. {'\n'} {'\n'} {'\n'} 

            <Text style = {{fontWeight: 'bold'}}>
              Class Meetings and Dismissal of Classes {'\n'} {'\n'}
            </Text>

            {'\t'} {'\t'} Punctuality and regularity should be observed in the classroom at all times in the
            conduct of classes. Teachers are expected to be present five minutes before the class
            starts. Likewise, classes should be dismissed at least five (5) minutes before the end of
            each period to allow students to move and transfer to their next class without delay. {'\n'} {'\n'} {'\n'}

            <Text style = {{fontWeight: 'bold'}}>
              Make-Up Classes {'\n'} {'\n'}
            </Text>

            {'\t'} {'\t'} Faculty members who miss their classes due to official functions should conduct
            make-up activities/classes. These activities/classes are free of charge and should be
            scheduled on another time that the students are available. {'\n'} {'\n'} {'\n'}

            <Text style = {{fontWeight: 'bold'}}>
              Suspension of Classes {'\n'} {'\n'}
            </Text>

            {'\t'} {'\t'} The University President has the sole authority to suspend classes in the
            university. In some special cases, however, the Dean of the College may suspend the
            classes in his/her unit provided that a report is submitted to the Office of the President
            stating the reasons for the suspension. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

ScheduleOfClass.propTypes = {

};

export default ScheduleOfClass;