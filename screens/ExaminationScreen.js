import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class ExaminationScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Examinations</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Attendance')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Grading System')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} Examinations are integral components of instruction and shall be administered by
              the faculty member subject to the College/Academic unit policies/rules for evaluating
              student performance. All examination papers should be checked, properly recorded, and
              must be returned to the students. {'\n'} {'\n'}
              
              {'\t'} {'\t'} Electronic devices such as cellphones, tablets, smart watches are generally not
              allowed during the examination, unless specified otherwise by the teacher. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Schedule of Examinations {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} There are three (3) periodic examinations prescribed every semester, namely;
              preliminary, midterm and final, which shall be conducted in accordance with the schedule
              prescribed under the University Calendar. {'\n'} {'\n'}

              {'\t'} {'\t'} Schedule of examinations may be changed by the course teacher provided it is
              approved by the Dean of College and the students concerned are informed one (1) week
              before. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Special Examinations {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Special examinations shall be given to students attending seminars, conventions,
              workshops, athletic and cultural competitions and the like during the time of examination
              if their participation is considered vital and approved by the Director of Student Services
              or the College Dean concerned. {'\n'} {'\n'}

              {'\t'} {'\t'} Students who missed the exam due to a medical condition or illness are allowed to
              take a special examination provided that they present a medical certificate. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Types of Examinations {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Faculty members enjoy the full academic freedom of deciding the type of
              examination to give to their students. It is suggested that the examinations assess the
              significant learning outcomes covered in the course particularly the Higher Order
              Thinking Skills (HOTS) such as creative and critical thinking skills. Performance-based
              examinations should be scored by using rubrics. {'\n'} {'\n'}

              {'\t'} {'\t'} Departments may also give departmental examinations in general education
              courses. The Dean assigns a committee who will prepare the examination for each
              course. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Preparation and Reproduction of Examination Questions {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} The individual faculty member prepares his/her own examination and uses the
              resources of the University, if available, to reproduce it. The students must not be made
              to pay for reproduction expenses except in urgent cases where reproduction was done
              outside the University resources and that the College Dean gave his/her permission. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Examination Proctors and Correctors {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Faculty members are the proctors and correctors of the examination given to their
              respective classes. Staff and students are not allowed as proctors and correctors. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Validating Examination {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} For purposes of accreditation of courses taken from other institutions by transfer
              students with ratings lower than 2.50, validating examinations may be requested by the
              student from the Office of the Dean. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Reporting Cases of Cheating and Other Forms of Dishonesty {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Any form of dishonesty and/or deceit, especially cheating during examinations or
              any class work, shall be subject to disciplinary action ranging from reprimand to
              automatic failure of the test or course. Students suspected of cheating will be reported to
              the Department Head who coordinates with the Director or Coordinator (for external
              campuses) of the Office of Student Services (OSS) through the College Dean for an
              investigation and the corresponding appropriate sanction. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

ExaminationScreen.propTypes = {

};

export default ExaminationScreen;