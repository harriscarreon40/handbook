import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class UniversitySymbols extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
            <Left>
              <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
                this.props.navigation.openDrawer()} />
            </Left>
            <Body>
              <Text>University Symbols</Text>
          </Body>
            <Right>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                  onPress={() => navigate('Present Times')}               
                />
              </Button>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                  onPress={() => navigate('USeP Hymn')}               
                />
              </Button>
            </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Image style = {{ alignSelf: 'center'}}
              source = {require('../images/usep_logo.jpg')}
            />
            
            <Text style = {{fontWeight: 'bold', alignSelf: 'center'}}>
              {'\n'} {'\n'} University Seal {'\n'} {'\n'}
            </Text>

            <Text style = {[styles.title]}>
              Symbols and Meanings {'\n'} {'\n'}
            </Text>

            <Text >
              <Text style = {[styles.title]}>
                Mount Apo - {' '}
              </Text>
                Found in Region XI, Mount Apo is considered the tallest mountain in the Philippines and is the cultural pride and heritage of the people of the south. {'\n'} {'\n'}
            </Text>

            <Text >
              <Text style = {[styles.title]}>
                Philippine Eagle - {' '}
              </Text>
                A very rare species among Philippine raptors, the monkey-eating eagle can only be found around the forest covering Mt. Apo and is now considered the bird that symbolizes southeastern Philippines. The majestic bird which can fly at high
                altitudes is included in the list of endangered species in the world. Experts have estimated
                that there are now few living species. {'\n'} {'\n'}
            </Text>

            <Text >
              <Text style = {[styles.title]}>
                Man - {' '}
              </Text>
                Humans are the prime movers of all endeavors. They carry out the objectives of this institution for a holistic development of human persons. {'\n'} {'\n'}
            </Text>

            <Text >
              <Text style = {[styles.title]}>
                Five Rays - {' '}
              </Text>
                These five rays represent the five main provinces of the region, namely the three Davao provinces, Surigao Sur and South Cotabato where the core of the institution is to be developed and enhanced. {'\n'} {'\n'}
            </Text>

            <Text >
              <Text style = {[styles.title]}>
                Courses of Endeavors - {' '}
              </Text>
                Instruction, research and extension work encircle the figure of man, the prime mover of all endeavors. {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>

            <Image style = {{ alignSelf: 'center'}}
              source = {require('../images/eagle.jpg')}
            />

            <Text>
              {'\n'} {'\n'} {'\t'} {'\t'} The Philippine Eagle monument found at the university entrance embodies the ideals and aspirations of the University of Southeastern Philippines. It represents the rare qualities of those who belong to the first state university in Region XI. Its capacity to reach high altitudes also signifies USeP’s far-reaching vision of the future while its talons exemplify the university’s strong conviction to carry out its mission. The legendary height of Mt. Apo stands for the strong will of the University's constituents to rise above the ordinary. Its breadth indicates USeP's desire to cover all the region's concerns. {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>
              
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  },
  title: {
    fontWeight: 'bold'    
  }
});

UniversitySymbols.propTypes = {

};

export default UniversitySymbols;
