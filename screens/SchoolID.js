import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class SchoolID extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>School Identification Card</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('School Uniforms')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Student Conduct')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} a) The University issues Identification Cards (ID) to all bonafide students of the
              university and must be validated at the beginning of every semester. {'\n'} {'\n'}

              {'\t'} {'\t'} b) Students are expected to wear their IDs all the time except during physical fitness activities. {'\n'} {'\n'}

              {'\t'} {'\t'} c) Lost IDs may be replaced after submitting a letter of request and an affidavit of
              loss to the Office of the Student Services (OSS). Furthermore, damaged IDs must
              be surrendered and replaced right away for it will not be honoured for any
              transaction. {'\n'} {'\n'}

              {'\t'} {'\t'} d) Security guards are instructed to check student IDs. All IDs confiscated with
              justifiable reasons may be claimed from the Chief of Security. {'\n'} {'\n'}

              {'\t'} {'\t'} e) The university ID is nontransferable and must be surrendered to the Office of the
              University Registrar (OUR) after graduation or upon withdrawal from the
              university. {'\n'} {'\n'} {'\n'}
            </Text>

            <Text style = {{fontWeight: 'bold', textAlign: 'center'}}>
              Curfew {'\n'} {'\n'}               
            </Text>

            <Text>
              {'\t'} {'\t'} To ensure the safety and protection of students, a curfew hour must be observed.
              Students will no longer be allowed to stay in the university premises after the following
              hours: {'\n'} {'\n'}
            </Text>

            <Text style = {{textAlign: 'center'}}>
              Bislig Campus - 6:00 PM {'\n'} 
              Mintal Campus - 8:00 PM {'\n'} 
              Mabini Campus - 9:00 PM {'\n'} 
              Obrero Campus - 10:00 PM {'\n'} 
              Tagum Campus - 6:00 PM {'\n'} {'\n'}   
            </Text>

            <Text>
              {'\t'} {'\t'} For special events and occasions, an approved letter requesting for an extension of time
              beyond the curfew hours must be submitted to the chief of security at least two (2) days              
              before the event. It should duly signed by the Office of the Student Services Director, the
              Vice-President for Academic Affairs and the University President. {'\n'} {'\n'} {'\n'}
            </Text>

            <Text style = {{fontWeight: 'bold', textAlign: 'center'}}>
              Field and Educational Trips {'\n'} {'\n'}               
            </Text>

            <Text>
              {'\t'} {'\t'} a) The faculty member has written a formal request to hold educational/field
              trips, stating clearly the objectives and the significance of the trip to the course
              or program. The letter must include the planned travel itinerary. This must be
              filed at the Office of the Dean at least one (1) month before the proposed
              schedule of the trip. Students must secure a certificate of insurance from the
              local Campus Student Council. {'\n'} {'\n'}

              {'\t'} {'\t'} b) Local trips that are beneficial to the training of students are given priority over
              long distance trips. This is to minimize time and financial expenses. {'\n'} {'\n'}

              {'\t'} {'\t'} c) Educational field trips are limited to only one (1) per semester per class.
              Consideration may be made within programs, thus recommendation from the
              Department Chairman or Program Head is necessary. {'\n'} {'\n'}

              {'\t'} {'\t'} d) Students participating in the trip should secure a waiver duly signed by the
              parent/guardian at least one (1) week before the scheduled departure. {'\n'} {'\n'}

              {'\t'} {'\t'} e) Students are expected to shoulder all expenses of the trip. {'\n'} {'\n'}

              {'\t'} {'\t'} f) Educational field trips are not compulsory. Students may opt not to join. In its
              place, the faculty member should give an alternative activity or homework to
              those who will not be joining the trip. {'\n'} {'\n'}

              {'\t'} {'\t'} g) The faculty member who organized the trip will accompany the students all throughout the trip. {'\n'} {'\n'}

              {'\t'} {'\t'} The University may impose additional requirements on educational/field trips to
              ensure the utmost safety of the participating students and faculty. {'\n'} {'\n'}  {'\n'} {'\n'}  {'\n'} {'\n'}

            </Text>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

SchoolID.propTypes = {

};

export default SchoolID;