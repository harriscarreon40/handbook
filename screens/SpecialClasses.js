import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class SpecialClasses extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Special Classes</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Schedule of Class')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Credit Unit')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>

              <Text style = {{fontWeight: 'bold'}}>
                Undergraduate {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} A {' '}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Special Class {' '}
              </Text>

              is a class requested by the students to be conducted for a specific
              course in the curriculum that is not part of the regular offering for that particular semester
              or summer. In some cases, the course may be part of the regular offering in a particular
              semester but the students, especially those who had failed in that course or had previously
              dropped it, are requesting it to be held at another time. {'\n'} {'\n'}

              {'\t'} {'\t'} The request, which shall be approved by the Office of the Vice President for
              Academic Affairs upon recommendation of the Dean, shall be initiated by the students.
              Only students who have failed or had previously dropped the requested course must be
              permitted to enroll. {'\n'} {'\n'}

              {'\t'} {'\t'} In all special classes, a minimum length of 18 hours per term, inclusive of
              examinations, corresponds to one (1) unit of collegiate academic credit. Unless approved
              by the Office of the VPAA, any arrangement to compress the schedule of classes or to
              reduce actual student contact time is not allowed. {'\n'} {'\n'}

              {'\t'} {'\t'} Each special class should comprise a minimum of thirty (30) students. If the
              number is smaller upon enrolment, the course may still be officially offered provided the
              enrolees equally share the regular amount of tuition, laboratory (if any), and other
              obligatory fees. Late or additional enrollees shall likewise pay the same amount shared by
              the members of the requesting group. {'\n'} {'\n'}

              {'\t'} {'\t'} Special class fees are paid in full upon enrollment. This means that the group that
              requested for the approved special class will pool together their payments and pay the full
              amount at once. No privilege of free tuition and other fees will be given to scholars and
              dependents. All fees follow the rates prescribed under the Evening Program scheme and
              shall adopt the corresponding charges stipulated for the year level where the requested
              course/subject is being offered. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Graduate {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'}For requested courses in the graduate programs, the guidelines to offer a special
              class follow the procedure and payment scheme stipulated under the undergraduate
              program. {'\n'} {'\n'}

              {'\t'} {'\t'} For courses under the regular offering of the program in a particular semester, the
              minimum class size to officially offer a graduate course is fifteen (15) and ten (10)
              enrollees, for core/basic and major/advance courses, respectively. If the number of
              enrollees is less than the required size, the course may still be offered as a special class.
              In this case, the official enrolees of the course shall prorate among themselves the
              aggregate payment of tuition that is equivalent to the required number of students. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Summer Special Classes for the Undergraduate Program {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} The following guidelines must be followed in the offering of summer special classes: {'\n'} {'\n'}

              {'\t'} {'\t'} a) The minimum number of students per class is thirty (30). Subjects with less than
              thirty (30) students may be offered provided the enrollees pay for the tuition and
              other fees equivalent to the total projected revenue. {'\n'} {'\n'}

              {'\t'} {'\t'} b) Tuition fee for all special classes is P250/unit. {'\n'} {'\n'}

              {'\t'} {'\t'} c) Students pay their special class accounts in full upon enrollment. Enrollment shall
              be in group per subject. No privileges will be given to scholars and dependents. {'\n'} {'\n'}

              {'\t'} {'\t'} d) The rule on pre-requisites will be strictly observed. Students are not allowed to
              enroll the requisite and the pre-requisite subjects simultaneously. {'\n'} {'\n'}

              {'\t'} {'\t'} e) Only students who have failed the requested subject or those who have failed to
              take it due to failure/failures in prerequisite subject/s are allowed to enroll. {'\n'} {'\n'}

              {'\t'} {'\t'} f) The maximum load of a student for the summer term shall be nine (9) units. In no
              case shall he/she be allowed to enroll more. {'\n'} {'\n'}

              {'\t'} {'\t'} g) The regular summer program will last for six (6) consecutive weeks covering 54
              hours. Any arrangement to shorten classes is prohibited. {'\n'} {'\n'}

              {'\t'} {'\t'} h) The Dean is responsible in assigning the faculty member to handle the special
              class taking into account his/her qualifications, availability, summer work
              load/administrative designation, and any ethical issues surrounding the
              assignment. {'\n'} {'\n'}

              {'\t'} {'\t'} i) The offering of all requested subjects, including that of external campuses, must
              be approved by the Vice President for Academic Affairs. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

SpecialClasses.propTypes = {

};

export default SpecialClasses;