import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class CreditUnit extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
     <View>
       <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Credit Unit</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Special Classes')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Attendance')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} One (1) unit of collegiate academic credit is equivalent to at least eighteen (18)
              full hours of instruction per semester in the form of lectures, discussions, seminars,
              tutorials, oral recitations, field work, examinations or any combination of these activities. {'\n'} {'\n'}

              {'\t'} {'\t'} For laboratory classes, one (1) unit of academic credit is equivalent to at least
              fifty-four (54) contact hours in a semester or three (3) contact hours per week. {'\n'} {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                ACADEMIC LOAD {'\n'} {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} A graduating student may be granted an overload in his/her last semester,
              provided that the total allowed academic load does not exceed the maximum of thirty
              (30) units including laboratory. {'\n'} {'\n'}

              {'\t'} {'\t'} A regular student shall enroll all the subjects prescribed in every semester or term
              for the curriculum to which he/she belongs. He/she shall carry a load of not more than
              the number of units prescribed per semester. {'\n'} {'\n'}

              {'\t'} {'\t'} A student who fails in at least one (1) subject will not be permitted to carry more
              subjects or academic units during the following semester than the allowed academic load. {'\n'} {'\n'}

              {'\t'} {'\t'} During the summer term, the normal load shall be six (6) units, but in justifiable
              cases, the Dean of the College may allow a greater load that will not exceed nine (9)
              units. {'\n'} {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                MEDIUM OF INSTRUCTION {'\n'} {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} English is generally used as a medium of instruction in the University. In
              consonance with the Bilingual Education Policy, a language course, whether Filipino or
              English, should be taught in the target language. Literature, Humanities and Social
              Science courses may be taught in Filipino, English or any other language, as long as there
              are enough instructional materials and both students and teacher are competent in that
              language. {'\n'} {'\n'}  {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
     </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

CreditUnit.propTypes = {

};

export default CreditUnit;