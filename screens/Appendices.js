import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class Appendices extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text> Appendices </Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Student Duties and Responsibilities')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>

            <Image 
              source = {require('../images/names.jpg')}
            />

            <Text>
              {'\n'} {'\n'}
            </Text>

            <Image 
              source = {require('../images/appendixB.png')}
            />

            <Image 
              source = {require('../images/appendixC.png')}
            />

            <Text>
              {'\n'} {'\n'}
            </Text>

            <Image 
              source = {require('../images/appendixD.png')}
            />

            <Image 
              source = {require('../images/appendixE.png')}
            />

            <Image 
              source = {require('../images/appendixF.png')}
            />

          </View>
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

Appendices.propTypes = {

};

export default Appendices;