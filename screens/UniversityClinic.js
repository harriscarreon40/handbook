import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class UniversityClinic extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>University Clinic</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('UGTO')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('ULRC')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} The University Health Service envisions itself to be the leader in health
              maintenance in providing basic health care to all its constituents by meeting the needs of
              each and every patient not only in the primary treatment of ailments and injuries but also
              in the prevention of illness through easy access of health care and education for each one
              in the realm of good health practices and behavior. Furthermore it aims to be the provider
              of holistic health care for all University constituents by: {'\n'} {'\n'}

              {'\t'} {'\t'} 1) Providing basic health care services in both medical and dental fields in cases
              of immediate and intermediate needs of the clients. {'\n'} {'\n'}

              {'\t'} {'\t'} 2) Promoting fitness and well being through health education on diseases and
              illnesses and health maintenance values aimed towards prevention rather than
              treatment. {'\n'} {'\n'}

              {'\t'} {'\t'} The University Health Service Division is composed of the medical and dental
              sections. The Medical Section is manned by a licensed physician and two (2) public
              health nurses. It offers free consultation, emergency medications and first-aid treatment
              of injuries and minor cuts. The patient/clientele of the division includes the faculty, staff
              and all students of the University. The student/patient is entitled to a free initial dose of
              medicine depending on his illness and availability of supply. The Medical Officer or the
              Physician examines all first year enrollees and issues medical clearance for enrolment.
              The Physician also issues medical clearances for other purposes e.g. to join various clubs
              of the University, for on-the-job-training of Technology and Engineering students and for
              new employees of the University, including the leave of absences of all employees and
              faculty. The dependents of a University employee can have free consultation but are not
              entitled to the free initial medication given to each constituent. {'\n'} {'\n'}

              {'\t'} {'\t'} The Dental Section is composed of a licensed dentist and a dental aide. It offers
              free tooth extraction and dental filling for all faculty, staff and students, excluding the              
              dependents. A student, however, is limited only to a once-every-semester dental
              treatment in order to accommodate as many students as possible. {'\n'} {'\n'}

              {'\t'} {'\t'} The Health Service Division derives its budget from the University Allotment
              (Fund 101) and from the income (Fund 164) of the medical fee of all students for its
              supplies and equipment. The Health Team travels to the different campuses of the
              University specifically the CDM-Mintal, Apokon, Mabini and Bislig Campus once a
              month or as the need arise to perform medical and dental treatment to the constituents of
              each campus. The University Clinic is only open from Mondays to Saturdays from 8:00
              a.m. to 5:00 p.m. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Medical Services {'\n'} {'\n'}               
              </Text>

              1. Free consultation and treatment. {'\n'} {'\n'} 
              2. Treatment of emergency cases whenever necessary. {'\n'} {'\n'} 
              3. Sustaining of minor cuts and wound. {'\n'} {'\n'} 
              4. Referral to hospitals for severe cases. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Dental Services {'\n'} {'\n'}               
              </Text>

              1. Dental Check-up {'\n'} {'\n'} 
              2. Tooth extraction {'\n'} {'\n'} 
              3. Tooth Filling {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}            

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

UniversityClinic.propTypes = {

};

export default UniversityClinic;