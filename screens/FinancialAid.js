import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class FinancialAid extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Financial Aid</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('OSAS')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Student Government, Campus Organizations and Publication')}               
              />
            </Button>
          </Right>
        </Header>
        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              <Text style = {{fontWeight: 'bold'}}>
                Policy Statement {'\n'} {'\n'}             
              </Text>

              {'\t'} {'\t'} In line with the constitutional mandate to establish and maintain a system of
              scholarship grants and other incentives which shall be available to deserving students,
              especially to the under-privileged, the University has consistently implemented one of its
              major functions enunciated in B.P. Bilang 12 which is to offer scholarship/or part-time
              job opportunities to deserving students from low-income families. Consequently, the
              university shall , likewise, implement state-supported scholarship under CHED, DOST,
              CSC, and other agencies. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Coverage {'\n'} {'\n'}             
              </Text>

              {'\t'} {'\t'} These guidelines apply to students enrolled in regular/day classes only. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Definitions of Terms {'\n'} {'\n'}             
              </Text>

                1. Acknowledged Children - Refer to children so certified by a faculty/staff in the University as his/her own; {'\n'} {'\n'}

                2. Division - Refers to an administrative unit headed by a director. {'\n'} {'\n'}

                3. Elected-at-large - Applies to student government officials who are elected to perform campus-wide duties/responsibilities. {'\n'} {'\n'}

                4. Immediate environs - Refer to places situated in city/provincial boundaries including Samal Island. {'\n'} {'\n'}

                5. Public school - Refers to DECS schools, TESDA and CHED supervised schools. {'\n'} {'\n'}

                6. Matriculation fee - Refers to amount as registration fee paid by the student {'\n'} {'\n'}

                7. Miscellaneous fees - Refer to the amount paid for laboratory fee, medical dental fee, guidance fee, library fee and others {'\n'} {'\n'}

                8. Non-permanent employee - Refers to faculty/staff whose appointment is either temporary, substitute, part-time or contractual.s {'\n'} {'\n'}

                9. Sanctionable acts - Refers to any offences enumerated in the Student Handbook. {'\n'} {'\n'}

                10. Tuition fee - Refers to amount paid for total units enrolled in the semester. {'\n'} {'\n'} {'\n'}

                <Text style = {{fontWeight: 'bold'}}>
                  Academic Scholarships {'\n'} {'\n'}             
                </Text>

                a. University Scholarship {'\n'} {'\n'}

                {'\t'} {'\t'} 1. This scholarship shall be granted to any student who obtains a grade
                weighted average (GWA) of 1.25 or better at the end of the semester
                in academic subjects in which he/she has enrolled the full academic
                load required in his/her curriculum. {'\n'} {'\n'}

                {'\t'} {'\t'} 2. Students in University Scholarship shall enjoy the following benefits: {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} a. Exemption from payment of tuition and miscellaneous fees
                except those assessment made by the student government and
                the school organ. {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} b. Book allowance of Php 500.00 per semester {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} c. Monthly stipend of Php 1,000.00 except during summer session {'\n'} {'\n'}

                {'\t'} {'\t'} 3. Students in the University Scholarship must not have any failing grade. {'\n'} {'\n'}

                {'\t'} {'\t'} 4. Failure on the part of the scholar to maintain the required GWA
                automatically terminates his/her scholarship in the succeeding
                semester. {'\n'} {'\n'} {'\n'}

                b. College Scholarship {'\n'} {'\n'}

                {'\t'} {'\t'} 1. College Scholarship shall be granted of 1.45 or better at the end of the
                semester in the subjects in which he/she has enrolled provided he/she
                has enrolled the full academic load required in the curriculum {'\n'} {'\n'}

                {'\t'} {'\t'} 2. Recipients of College Scholarship shall enjoy the following benefit {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} a. Exemption from payment of tuition and miscellaneous fees
                except those assessment made by the student government and
                school organ. {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} b. Book allowance of Php 500.00 per semester {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} c. Monthly stipend of Php 700.00 except during summer session {'\n'} {'\n'}

                {'\t'} {'\t'} 3. Tuition Privilege: This scholarship exempts a student who passes all subjects in the
                academic load with a minimum of 18 units or regular load
                prescribed in the curriculum provided the student obtains GWA of
                1.75 or better. {'\n'} {'\n'}

                {'\t'} {'\t'} 4. Failure on the part of the scholar to maintain the required GWA
                automatically terminates his/her scholarship in the succeeding
                semester. {'\n'} {'\n'} {'\n'}

                c. Entrance Scholarship {'\n'} {'\n'}

                {'\t'} {'\t'} 1. CThis scholarship is awarded to any high school valedictorian and
                salutatorian who are admitted to the university. Entrance scholars shall
                be exempted from payment of tuition. {'\n'} {'\n'}

                {'\t'} {'\t'} 2. This scholarship is maintained with a GWA of 1.75 or better of the
                regularly credited academic load in the previous semester as
                prescribed in the curriculum. {'\n'} {'\n'}

                {'\t'} {'\t'} 3. Failure on the part of the scholar to maintain the required GWA
                automatically terminates his/her scholarship in the succeeding
                semester. {'\n'} {'\n'} {'\n'}

                <Text style = {{fontWeight: 'bold'}}>
                  Grants {'\n'} {'\n'}             
                </Text>

                {'\t'} {'\t'} Grants are awarded to deserving students who passed the USePAT and are not
                enjoying any scholarship or privilege at the time of application. Recipients of these grants
                are entitled to free tuition.  {'\n'} {'\n'}

                {'\t'} {'\t'} 1. Music {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} a. Band Members - Awarded to students who are regular members of the band
                and recommended by the band master.{'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} b. University choir members {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} c. Candidates for University choir shall undergo audition before an appropriate committee created by the University President. {'\n'} {'\n'}

                {'\t'} {'\t'} 2. University Dance Troupe {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} Recommended by the audition committee, membership is awarded
                to those who qualified in the selection process. Regular members
                have their main counterpart of promoting cultural shows in the
                University and shall provide free choreographic services to any
                college in need and to the duly recognized club/organizations.{'\n'} {'\n'}

                {'\t'} {'\t'} 3. Athletics {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} Awarded to the following students: {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} a. Incoming freshmen who are medalists in at least regional level
                athletic competitions as certified by the athletic manager or
                head of school. {'\n'} {'\n'}

                {'\t'} {'\t'} {'\t'} {'\t'} b. Those who are already in the University who have qualified as
                varsity players as recommended by an appropriate selection
                committee created by the University President.
                Recipients can maintain these grants subject to the following
                conditions: a) they must carry an academic load of at least 18
                units. B) that the students major concern in the University
                must be the pursuit of his academic program. {'\n'} {'\n'} {'\n'}

                <Text style = {{fontWeight: 'bold'}}>
                  Student Labor {'\n'} {'\n'}             
                </Text>

                {'\t'} {'\t'} Each College and/or division is entitled to not more than 5 and 3 student
                laborers, respectively except the University library and OUR which may
                have more slots and approval of which shall be sought with the Office of
                the President of the University. External campuses may be allotted a
                number of student labors to be determined by the dean, subject to approval
                of the President. Each student laborer is paid Php10.00 per hour. {'\n'} {'\n'} {'\n'}

                <Text style = {{fontWeight: 'bold'}}>
                  Termination of Benefits {'\n'} {'\n'}             
                </Text>

                {'\t'} {'\t'} 1. The University reserves its right to terminate any scholarship/grant/or
                privilege with any recipient on the basis of sanctionable acts committed by
                him/her as may be determined by an appropriate committee created by the
                University President. As a matter of rule, due process must be observed. {'\n'} {'\n'}

                {'\t'} {'\t'} 2. The University can terminate any scholarship/grants/privilege for nonavailability of funds. {'\n'} {'\n'} {'\n'}

                <Text style = {{fontWeight: 'bold'}}>
                  Other Ruless {'\n'} {'\n'}             
                </Text>

                {'\t'} {'\t'} 1. Only one scholarship of the University can be enjoyed by a student at time. {'\n'} {'\n'}

                {'\t'} {'\t'} 2. Scholarship/privileges and grants are strictly in accordance with the
                existing rules established by the scholarship committee and that of the
                sponsoring agency for private scholarship grants. {'\n'} {'\n'}

                {'\t'} {'\t'} 3. Applicants must not have an incomplete grades shall, 15 days from date of
                enrollment, be completed before filing an application subject to the rules
                of the scholarship committee. {'\n'} {'\n'}

                {'\t'} {'\t'} 4. Sponsors of private scholarship shall execute a memorandum of agreement with the University. {'\n'} {'\n'}

                {'\t'} {'\t'} 5. Scholarship/grantees and beneficiaries including student assistants shall be
                entitled for full refund of their tuition, matriculation, or miscellaneous
                fees, as the case maybe, should they incur advance payments. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

FinancialAid.propTypes = {

};

export default FinancialAid;