import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class HonorableDismissal extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Honorable Dismissal</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Scholastic Delinquency')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('School Tuition and Miscellaneous Fees')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} A student who desires to leave the University shall present a written petition to
              this effect to the University Registrar, signed by his parent or guardian. If the petition is
              granted, the student shall be given Certificate of Honorable Dismissal upon presentation
              of his/her clearance and receipt of payment. The certificate indicates that the student
              withdrew in good standing as far as character and conduct are concerned. If the student
              has been dropped from the rolls because of poor scholarship, a statement to that effect
              may be added to the honorable dismissal. {'\n'} {'\n'}

              {'\t'} {'\t'} After the release of the Certificate of Honorable Dismissal, incomplete grades
              obtained by the student can no longer be removed even if those grades are still within the
              reglementary period. This rule likewise applies to students who have already registered in
              another school before being officially granted honorable dismissal by the University. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Dismissal {'\n'} {'\n'}              
              </Text>

              {'\t'} {'\t'} A student who leaves the University for reasons of suspension, dropping or
              expulsion due to misconduct is entitled, or permitted to receive the transcript of records
              or certification of his/her academic status in the University. The document will contain a
              statement about the disciplinary action rendered against him/her. All debts to the
              University must be settled before such documents will be issued to him/her. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>
          </View>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

HonorableDismissal.propTypes = {

};

export default HonorableDismissal;