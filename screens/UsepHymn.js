import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class UsepHymn extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
            <Left>
              <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
                this.props.navigation.openDrawer()} />
            </Left>
            <Body>
            <Text>USeP Hymn</Text>
          </Body>
            <Right>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                  onPress={() => navigate('University Symbols')}               
                />
              </Button>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                  onPress={() => navigate('Organizational Structure')}               
                />
              </Button>
            </Right>
          </Header>

          <View style = {[styles.container]}>            
            <Text style = {{textAlign: 'center'}}>
              USeP NAMING LIYAG {'\n'} {'\n'}
            </Text>
            
            <Text style = {[styles.content]}>
              Tanging Pamantasan naming mahal, {'\n'}
              Tampok ng Timog Mindanao {'\n'}
              Ang liwanag mo ay aming ilaw, {'\n'}
              Sa landas ng karunungan. {'\n'} {'\n'}

              USeP naming liyag {'\n'}
              Nasa ‘yo ang pangarap {'\n'}
              Ang pag-asa at pag-unlad, {'\n'}
              Dunong laya at galak {'\n'}
              Ng mga kabataang s’yang lakas {'\n'}
              Nitong bayan, ngayo’t bukas {'\n'} {'\n'}

              Pamantasan naming dakila {'\n'}
              Batis ka ng dunong at tuwa {'\n'}
              Ang pangalan mo’y ibabandila {'\n'}
              Ng matanghal itong bansa {'\n'} {'\n'}

              USeP pamantasan naming liyag {'\n'}
              Handog sayo’y puri at bulaklak {'\n'}
              Ang Pangalan mo ay itatanyag {'\n'}
              Bandila mo’y itataas {'\n'} {'\n'}

              Ang pangalan mo ay itatanyag {'\n'}
              USeP naming liyag. {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>

          </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  },
  content: {
    alignItems: 'center',
    textAlign: 'center',
    fontStyle: 'italic'
  }
});

UsepHymn.propTypes = {

};

export default UsepHymn;