import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class RegistrationScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Registration</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Admission')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Academic Courses')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
            {'\t'} {'\t'} A student must be officially registered in order to receive credit for course work.
            The schedule for registration is indicated in the annual university calendar and if any
            student registers outside of the regular dates, he/she will have have to pay a late
            registration penalty. Students will no longer be allowed to register two (2) weeks after
            regular undergraduate/graduate classes have started. However, students enrolling in
            special programs with no prescribed schedule of enrollment may register at any time
            without having to pay a late registration fine, subject to other regulations of the
            University. {'\n'} {'\n'}
            </Text>

            <Text style = {{fontWeight: 'bold'}}>
                Cross Registration {'\n'} {'\n'}
            </Text>
              
            <Text>
              <Text style = {{fontStyle: 'italic', fontWeight: 'bold'}}>
              {'\t'} {'\t'} Within the university – {' '}
              </Text>
            
              Students wishing to register in a College/Academic Unit
              of the University other than where they are primarily enrolled should ask permission
              from their Dean or Director of the College/Academic Unit. They will accomplish a form
              for cross-registration purposes. Students requesting for permission to cross-register for
              courses in another College/Academic Unit should first complete his/her registration
              (including payment of fees) in the College/Academic Unit where he/she is primarily
              enrolled. The total number of units of credit for which a student may register in two or
              more colleges/academic units in this University should not exceed the maximum number
              allowed in the rules on academic load. {'\n'} {'\n'}

              <Text style = {{fontStyle: 'italic', fontWeight: 'bold'}}>
              {'\t'} {'\t'} To another institution - {' '}
              </Text>

              The University gives no credit for any course taken by
              any of its students in any other institution unless taking such a course was duly approved
              by the Dean upon recommendation of the University/Campus Registrar. The written
              authorization is recorded by the University Registrar and should specify the courses
              authorized. {'\n'} {'\n'}

              <Text style = {{fontStyle: 'italic', fontWeight: 'bold'}}>
              {'\t'} {'\t'} From another institution - {' '}
              </Text>

              A student who registered in another institution and
              who wishes to cross-register in USeP must present a written permit from his/her Dean or
              Registrar. The written permit should state the total number of units for which the student
              is registered and the courses that he/she is authorized to take in the University. {'\n'}  {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Regulations on Cross-Enrolment to other Higher Education Institutions (HEI): {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} a) Students are not allowed to cross-enroll major subjects of their curriculum
              except when the subject is not offered in the University in the final semester
              of his/her studies. {'\n'} {'\n'}

              {'\t'} {'\t'} b) Students may cross-enroll General Education courses, subject to the approval
              of the Dean of their College, only if there is a conflict of schedule. {'\n'} {'\n'}

              {'\t'} {'\t'} c) Non-graduating students are allowed to cross-enroll only if the subject to be
              enrolled is not a major course of their curriculum. {'\n'} {'\n'}

              {'\t'} {'\t'} d) Students are not allowed to cross-enroll to more than one (1) HEI. {'\n'} {'\n'}

              {'\t'} {'\t'} e) The HEI where the student may cross enroll should have achieved Level III
              status for State Universities and Colleges (SUCs) or private institutions that
              are either deregulated or autonomous. {'\n'} {'\n'}

              {'\t'} {'\t'} f) Students are allowed to cross-enroll a maximum of six (6) academic units per semester. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Shifting of Program Concentration {'\n'} {'\n'}
              </Text>

              {'\t'} {'\t'} Students currently enrolled in the university who wish to transfer to another
              program should file at his/her current college an application or request for transfer. The
              request must be accompanied by a recommendation from the Guidance Counsellor. Applications shall be referred to the accepting college together with a certification of grades from the OUR that contains his/her scholastic record. {'\n'} {'\n'}

              {'\t'} {'\t'} If the student satisfies the admission requirements of the accepting College,
              he/she submits University clearance to the college/academic unit she wishes to transfer to
              along with the permit to transfer and certificate of grades or certified evaluation sheet. {'\n'} {'\n'}

              {'\t'} {'\t'} Students are not allowed to enroll in two (2) academic programs at the same time. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

RegistrationScreen.propTypes = {

};

export default RegistrationScreen;