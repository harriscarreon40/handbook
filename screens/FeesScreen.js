import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class FeesScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>School Tuition and Miscellaneous Fees</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Honorable Dismissal')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('School Uniforms')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} Tuition fees and other fees must be reflected upon the receipt and certificate of
              registration and billing (CORB) issued by the University Finance Department.
              Furthermore, as stipulated by the financial code of the university, all refundable fees must
              be returned to the students. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Refund of Fees {'\n'} {'\n'}              
              </Text>

              {'\t'} {'\t'} Only fully-paid tuition and miscellaneous fees shall be refunded in accordance with the prescribed schedule and regulation: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} i. Within one week from the opening of classes.......... 70% {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} ii. Within two weeks from the opening of classes………….. 50% {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} iii. Within three weeks from the opening of classes……….... 30%{'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} iv. Within four weeks from the opening of classes………..….20%{'\n'} {'\n'}

              {'\t'} {'\t'} After the fourth week or 30 days after the opening of classes, refund of tuition and
              other fees is no longer allowed. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>          
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

FeesScreen.propTypes = {

};

export default FeesScreen;