import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class Preface extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
            <Left>
              <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
                this.props.navigation.openDrawer()} />
            </Left>
            <Body>
              <Text>Preface</Text>
          </Body>
            <Right>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                  onPress={() => navigate('Message of the President')}               
                />
              </Button>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                  onPress={() => navigate('USeP VMGC')}               
                />
              </Button>
            </Right>
        </Header>
        <ScrollView>
          <ScrollView horizontal={true}>
            <Image style = {[styles.container]}
                source = {require('../images/preface.jpg')}
              />
          </ScrollView>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: -100
  }
});

Preface.propTypes = {

};

export default Preface;