import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class DelinquencyScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Scholastic Delinquency</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Policy on Student Records')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Honorable Dismissal')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} Students are expected to make satisfactory progress towards a degree, certificate
              or other approved program of study. To ensure that students are making progress, the
              University shall imposed suitable and effective provisions governing delinquent
              undergraduate students. These provisions are subject to the minimum standards: {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Warning {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Any student who, at the end of the semester, fails by twenty five percent (25%) of
              the total number of academic units for which he/she is enrolled will be officially warned
              by the adviser, through a written notice, to improve his/her work. In the succeeding
              semester he/she can still carry a normal academic load. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Probation {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} If a student fails again by 25% of the total number of academic units enrolled
              after the warning, he/she will be placed on probation. {'\n'} {'\n'}

              {'\t'} {'\t'} Any student who, at the end of the semester, obtains passing grades in less than
              50% of the total number of academic units for which he/she is enrolled will be placed on
              probation in the succeeding semester. {'\n'} {'\n'}

              {'\t'} {'\t'} A student on probation status will be allowed to carry only a limited load of not
              more than seventy five percent (75%) of the regular load for the semester. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Dismissal from the College/Academic Unit {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} {'\t'} {'\t'} a) Any student on probation who fails more than 50% of the total number of
              units in which he/she is enrolled will be dropped from the rolls of the
              College/Academic Unit. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} b) Any student who obtains three (3) failures in two (2) consecutive
              semesters will be dropped from the rolls of the College/Academic Unit. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} c) Any student who, at the end of the semester, fails more than 75% of the
              total number of academic units enrolled will be dropped from the rolls of
              the College/Academic Unit. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} d) Any student dropped from one College/Academic Unit will not
              ordinarily be considered for admission to another unit of the University
              unless, in the recommendation of the Guidance Counselor, his/her natural
              aptitude and interest may qualify him/her in another field of study. In
              this case he/she may be allowed to enroll in the proper College/Academic
              Unit or Department. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Permanent Disqualification {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Any student will be permanently barred from re-enrolment in any
              college/academic unit of the University on the following bases:  {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} a) Any student who, at the end of the semester or term, obtains failure in
              100 percent of the academic units in which he/she enrolled. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} b) Any student who was dropped in accordance with Item (iv) above of the
              rules on dismissal and again fails which make it necessary to drop him
              again, is no longer eligible for readmission. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} c) Dismissal due to violation of the rules and regulations of the University. {'\n'} {'\n'}

              {'\t'} {'\t'} Permanent disqualification will not apply to cases in which failing grades of the
              student are due to his/her unauthorized dropping of the courses and not to poor
              scholarship, as certified by the Department Head concerned. However, if the
              unauthorized withdrawal takes place after the mid-term examinations and the mid-term
              grades obtained by the student were poor, the grade of 5.0 (failure) will be credited
              against him/her for the purpose of this scholarship rule. The Dean will deal with these
              cases on their individual merits provided that in no case of readmission to the same or
              another college/academic unit will the action be lighter than probation. {'\n'} {'\n'}

              {'\t'} {'\t'} A grade of INC (incomplete) is not to be included in the computation. When it is
              replaced by a final grade, the latter is to be included in the grades during the semester
              when the removal or completion is made. {'\n'} {'\n'}

              {'\t'} {'\t'} No readmission of dismissed student or disqualified students will be considered
              by the College Deans without the favorable recommendation of the Director of the Office
              of Student Services. Cases in which the action of the College Dean conflicts with the
              recommendation of the Director of the Office of the Student Services may be elevated to
              the VP for Academic Affairs for his/her final decision. {'\n'} {'\n'}

              {'\t'} {'\t'} A student who fails a course three times will be advised to transfer to another
              academic institution. The same rules apply to a student who drops at least one (1) course
              for three (3) consecutive terms. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Enrollment of Failed Courses {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} A student will be advised to enroll first in required courses that he/she failed.
              Required courses will take precedence over other courses in his/her succeeding
              enrollment. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

DelinquencyScreen.propTypes = {

};

export default DelinquencyScreen;