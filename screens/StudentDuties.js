import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class StudentDuties extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Student Duties and Responsibilities</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Student Government, Campus Organizations and Publication')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Appendices')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} This is the confederation of all student councils from five campuses of USeP. This
              aims to develop closer relationship, camaraderie and advancement in terms of leadership
              as a form of social responsibility. {'\n'} {'\n'}

              • Strive to live an upright, virtuous and useful life. {'\n'} {'\n'}

              • Love, respect and obey his/her parents and cooperate with them to keep the family harmonious and united. {'\n'} {'\n'}

              • Exert his utmost effort to develop his/her potential for service, particularly by
              underlying an education suited to his/her abilities in order that he/she may become
              an asset to himself and to the society. {'\n'} {'\n'}

              • Respect the customs and traditions of our people, the duly constituted authorities,
              the laws of the country and the principles of democracy. {'\n'} {'\n'}

              • Participate actively in civic affairs for the promotion of general welfare. {'\n'} {'\n'}

              • Help in the observance and exercise of individual and social rights, the
              strengthening of freedom everywhere, the fostering of cooperation among nations
              in the pursuit of progress and prosperity and world peace. {'\n'} {'\n'}

              • Respect and cooperate with teachers, fellow students and school authorities in the
              attainment and preservation of order in school and in the society. {'\n'} {'\n'}

              • Exercise his/her rights and responsibility with due regard to the rights of others. {'\n'} {'\n'} {'\n'}
            </Text>

            <Text style = {{fontWeight: 'bold', textAlign: 'center'}}>
              STUDENT RIGHTS {'\n'} {'\n'}             
            </Text>

            <Text>
              Students of the university shall enjoy the following rights: {'\n'} {'\n'}

              • Students have the right to organize autonomous, democratic and representative
              student government. Student government has the right to determine its policies
              and programs on student activities, subject to the provisions of this handbook and
              the policies and regulations of the university. {'\n'} {'\n'}

              • Students have the right to participate in the governance of the university by
              having representatives to the policy-making bodies with voting powers coming
              from their ranks. Leading officer/s best represent the student body in these policymaking
              bodies. {'\n'} {'\n'}

              • Students have the right to establish clubs, organizations, alliances, and other
              association among themselves based on their general and particular interests like
              academic, religious, cultural and political organizations. {'\n'} {'\n'}

              • Students shall not be compelled to join or not to join any particular organization.
              Each student’s choice must be totally respected and no form of intimidation or
              harassment shall be tolerated. {'\n'} {'\n'}

              • Students have the right to publish regular student-controlled publication. No
              individual that is not a member or staff shall determine the content of the student
              publication. The editor shall take full responsibility for consequences arising from
              publication articles. The role of the faculty adviser if there is any shall be limited
              only to technical guidance. {'\n'} {'\n'}

              • Students have the right to print, circulate and/or mount leaflets, newsletters,
              posters, wall news petitions, and similar materials. {'\n'} {'\n'}

              • Students have the right to peaceful assembly and to petition for redress of
              grievances. No student shall be banned for participating an assembly. No student
              shall be arrested or detained for doing so. {'\n'} {'\n'}

              • No student shall be held to answer for any offense without due process. Students
              have the freedom from arbitrary arrest, from arbitrary seizures, and the right to
              resort to the writ of habeas corpus and to speedy, impartial and public justice. {'\n'} {'\n'}

              • Students have the right to reasonable bail and the freedom from double jeopardy,
              excessive fines and cruel or unusual punishment. {'\n'} {'\n'}

              • Students have the right to freedom from torture, threats, harassment, manhandling
              combined with interrogation, acts of terror and other means which vitiate free
              will. {'\n'} {'\n'}

              • Students have the right to freedom from unwarranted interference. No military
              detachments shall be installed inside the campus. Military elements shall not
              interfere with student activities. {'\n'} {'\n'}

              • Students have the right to procedural fairness in disciplinary proceedings. {'\n'} {'\n'}

              • Students have the right to accurate information. {'\n'} {'\n'}

              • Students have the right to hear any opinion or any subject of public concern,
              whether or not related to any subject they may be currently studying, which they
              believe is of worthy consideration. {'\n'} {'\n'}

              • Students have the right to free access of information on matters of public concerns. {'\n'} {'\n'}

              • Students have the right to free research in connection with academic work and the
              publication, discussion and exchange of findings and recommendations. {'\n'} {'\n'}

              • Students have the right to complete instruction and adequate welfare services and curricular facilities. {'\n'} {'\n'}

              • Every student has the right to receive relevant quality education in line with
              national goals, educational objectives and standards of the University. {'\n'} {'\n'}

              • Every student is entitled to guidance and counseling services to enable him to
              know himself, to make decisions and to select from the alternative in the fields of
              work in line with his potentialities. {'\n'} {'\n'}

              • Students have the right to participate in curricular and co-curricular activities. {'\n'} {'\n'}

              • Every student is entitled to be respected as a person with human dignity, to full
              physical, social, moral and intellectual development, to humane and healthy
              conditions of learning. {'\n'} {'\n'}

              • Students have the right to academic freedom as provided in the Constitution. {'\n'} {'\n'}

              •Every student has the right to access to class and other records for the purpose of
              determining his class standing and the University shall maintain and preserve
              such records.. {'\n'} {'\n'}

              • Every student officially admitted in the University has the right to continue and
              pursue his/her course of study therein up to graduation, except in the cases of
              academic delinquency and violation of disciplinary regulations.. {'\n'} {'\n'}

              • Students have the right to be assisted by the University through current and
              adequate information on work opportunities. {'\n'} {'\n'}

              • Students shall be entitled to expeditious issuance of certificates, diplomas,
              Transcript of Records, grades and Transfer Credentials. {'\n'} {'\n'}

              • Students have the right to receive medical and dental services as well as first-aid
              services. Students shall be provided with medical supplies for simple medication
              and have to provide for themselves medicines in cases of major illness. {'\n'} {'\n'}

              • Students shall enjoy other rights not mentioned herein. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

              
            </Text>
          </View>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

StudentDuties.propTypes = {

};

export default StudentDuties;