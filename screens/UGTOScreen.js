import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class UGTOScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>UGTO</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Committee on Student Discipline')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('University Clinic')}               
              />
            </Button>
          </Right>
        </Header>
        
        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} With the concerted efforts of the administration, faculty, staff, and the guidance
              personnel, the program's objectives will be achieved through the effectiveness and
              efficiency of the following services: {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Counseling Service {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} This service provides opportunity for the individual students to be assisted in
              areas of personal, vocational, social and academic concerns through personal and
              confidential relationship with qualified counselors. These could be availed through walkin,
              call-in, or referrals. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Testing {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} This involves the administration and interpretation of standardized tests
              for the purpose of assessing individual strengths and weaknesses in the areas of
              personality, aptitude, interest and motivation. Tests include mental ability, interest,
              aptitude, personality and other assessments and survey materials. {'\n'} {'\n'}

              {'\t'} {'\t'} Included in testing service is the USeP Admission test which is given to all
              incoming students months before the school year starts for the purpose of screening and
              evaluation.. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Information {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} The Office secures information and makes these available to students in response
              to their educational, occupational and socio-personal needs. These are in the form of
              brochures or articles from published reading materials. Bulletin boards for information
              are also provided in each college. Likewise, orientations, seminars, workshops and
              symposia are being conducted. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Group Guidance Sessions {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} These are group activities which tackle issues and concerns common to students,
              such as boy-girl relationships, adjustment to college life, how to study effectively,
              enhancing self-esteem, personality development, etc. Sessions like these are conducted in
              a non-threatening atmosphere. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Career Development {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} This is concerned with helping students make intelligent decisions regarding their
              goals in life, as well as planning and charting their career goals. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Placement and Follow-up {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Through this service, assistance is given to students to gain admission to certain
              colleges within the University and other schools for further education. Alumni members
              are also given assistance in seeking employment by posting job opportunities to meet
              several employers, undergo interviews at the same venue. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Peer Facilitating {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} This service aims to train selected students to develop their helping skills to
              effectively respond to the needs of their fellow students. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Individual Inventory {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} This involves continuous process of gathering pertinent data about students as
              basis for helping them understand themselves better and also for better responding to
              their needs. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Research and Evaluation {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} This carries out the systematic evaluation of the guidance services, the purpose of
              which is to find out if the program goals and objectives have been met. Furthermore, the
              result of these could be the basis for modifying or improving the delivery of the office’s
              services. {'\n'} {'\n'} {'\n'}
            </Text>

            <Text style = {{fontWeight: 'bold', textAlign: 'center'}}>
              STATEMENT OF COUNSELING CONFIDENTIALITY {'\n'} {'\n'}               
            </Text>

            <Text>
              {'\t'} {'\t'} Counseling session, the guidance counselor requires the counselee to sign a
              counseling contract indicating terms of agreement between the counselor and the
              counselee in working towards the resolutions of the latter’s concerns. It is imperative for
              the counselor to preserve and safeguard the confidentiality of the clients except in cases
              wherein disclosure is required to prevent clear and imminent danger to the client or
              others, or when legal requirements demand that confidentiality matter be revealed.            
              ( PGCA Code of Ethics, Chapter 2.) {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>


          </View>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

UGTOScreen.propTypes = {

};

export default UGTOScreen;