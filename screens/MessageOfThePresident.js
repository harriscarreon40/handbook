import React, { Component } from 'react';
import { StyleSheet, View, Image, ScrollView, Text } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'


class MessageOfThePresident extends Component {
  render() {    
    const {navigate} = this.props.navigation;
    return (
      <View > 
        <Header  style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
         <Body>
           <Text>Message of the President</Text>
         </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Student Pledge')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Preface')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <ScrollView horizontal={true}>
            <Image style = {[styles.fit]}
                source = {require('../images/message.jpg')}
              />
          </ScrollView>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fit: {
    flex: 1,
    marginLeft: -60
  },
});

MessageOfThePresident.propTypes = {

};

export default MessageOfThePresident;