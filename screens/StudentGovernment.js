import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class StudentGovernment extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Student Government, Campus Organizations and Publication</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Financial Aid')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Student Duties and Responsibilities')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              <Text style = {{fontWeight: 'bold'}}>
                Supreme Student Government (SSG) {'\n'} {'\n'}             
              </Text>
              
              {'\t'} {'\t'} This is the confederation of all student councils from five campuses of USeP. This
              aims to develop closer relationship, camaraderie and advancement in terms of leadership
              as a form of social responsibility. {'\n'} {'\n'}

              {'\t'} {'\t'} The Student Representative to the Board of Regents will also come from this
              confederation, through an election, among the student council presidents of all campuses. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Student Council {'\n'} {'\n'}             
              </Text>

              {'\t'} {'\t'} All campuses are free to create their own student government structure as long as
              it is in line with the university policies and provided that it should have its own principles
              and policies set in a constitution and by-laws (CBL). Furthermore, it should cater the
              needs of the students in terms of development, interest and activities that will hone them
              to become useful citizens in the future. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Student Publication {'\n'} {'\n'}             
              </Text>

              {'\t'} {'\t'} Student publication of all USeP campuses must circulate newspapers, magazines,
              and any other related releases concerning students’ activities, interests and information          
              that would be beneficial. These releases must be produce through the efforts of the
              students who have undergone screening, interviews and trainings on campus journalism. {'\n'} {'\n'}

              {'\t'} {'\t'} Publication should uphold press freedom and a catalyst of change may it be in
              thoughts, in words and in deeds for positive maturity. It also develops students’skills in
              writing and critical thinking in terms of perusing issues and opinions. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                University Clubs and Organizations {'\n'} {'\n'}             
              </Text>

              {'\t'} {'\t'} The university gives freedom to all students to create their own club or
              organization as long as it is in line with the university policies and uphold student rights.
              It should also be recognized through the Office of Student Services (OSS) for them to
              enjoy club privileges. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

StudentGovernment.propTypes = {

};

export default StudentGovernment;