import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class StudentPledgeScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
            <Left>
              <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
                this.props.navigation.openDrawer()} />
            </Left>
            <Body>
            <Text>Student Pledge</Text>
          </Body>
            <Right>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                  onPress={() => navigate('Home')}               
                />
              </Button>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                  onPress={() => navigate('Message of the President')}               
                />
              </Button>
            </Right>
          </Header>
        <View style={[styles.container]}>
          
                
          <Text style={[styles.textStyle]}>
              As a student privileged to be admitted to the University of
              Southeastern Philippines, I hereby promise to abide by all the
              rules and regulations laid down by its competent authority. 
              {"\n"} 
              {"\n"} 
              {"\n"}
          </Text>
          <Text> ___________________________________</Text>
          
          <Text style={[styles.textStyle]}>Signature Over Printed Name</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 40,
    paddingTop: 200
  },
  textStyle: {
    fontStyle: 'italic'
  }
});

StudentPledgeScreen.propTypes = {

};

export default StudentPledgeScreen;