import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class OSAS extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>OSAS</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('OUR')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Financial Aid')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              <Text style = {{fontWeight: 'bold'}}>
                Affidavit for Lost ID Cards, Receipts, Printouts and Study Loads {'\n'} {'\n'}             
              </Text>

              {'\t'} {'\t'} Schedule of Availability: Monday – Friday, 8:00am - 5:00pm {'\n'} {'\n'}

              {'\t'} {'\t'} Who may avail of the service: Students {'\n'} {'\n'}

              {'\t'} {'\t'} Requirements: Affidavit of Loss {'\n'} {'\n'}

              {'\t'} {'\t'} Duration: 5-6 minutes {'\n'} {'\n'}

              {'\t'} {'\t'} The Clerk In-charge assesses and verifies authenticity of said document. After which, the
              OSS Director/Clerk In-charge records and issues the Temporary Exemption Slip. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Filing of Complaint Against a Student or a Teacher {'\n'} {'\n'}             
              </Text>

              {'\t'} {'\t'} Schedule of Availability: Monday – Friday, 8:00 am - 5:00 pm {'\n'} {'\n'}

              {'\t'} {'\t'} Key Persons: Dean, Office of Student Affairs & Services {'\n'} {'\n'}

              {'\t'} {'\t'} Who may avail of the service: Anyone aggrieved or offended by a student/s {'\n'} {'\n'}

              {'\t'} {'\t'} Requirements: {'\n'} {'\n'}

              {'\t'} {'\t'} 1. Accomplished Incident Report Form also referred to as complaint form. {'\n'} {'\n'}

              {'\t'} {'\t'} 2. Full name of the student complained of and full name of person complaining. {'\n'} {'\n'}

              {'\t'} {'\t'} 3. A narration of relevant facts that show the offense allegedly committed by the student {'\n'} {'\n'}

              {'\t'} {'\t'} 4. complained of. {'\n'} {'\n'}

              {'\t'} {'\t'} 5. Evidence and testimonies of a witness/ess {'\n'} {'\n'}

              {'\t'} {'\t'} Duration: 15 minutes {'\n'} {'\n'}

              {'\t'} {'\t'} Once the requirements are done, the complainant orally reports and discusses with the
              OSS Counselor/s the situation . The OSS Counselor/s then documents complaint in a logbook.
              After which, the OSS Counselor/s receives the accomplished Incident Report Form. Finally,
              the OSASS/Dean informs complainant on the venue, date and time of dialogue/hearing and
              issues an official notice regarding the same. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Issuance of Certificate of Good Standing {'\n'} {'\n'}             
              </Text>

              {'\t'} {'\t'} Schedule of Availability: Monday – Friday, 8:00 am - 5:00 pm {'\n'} {'\n'}

              {'\t'} {'\t'} Key Person: Clerk {'\n'} {'\n'}

              {'\t'} {'\t'} Who may avail of the service: Students {'\n'} {'\n'}

              {'\t'} {'\t'} What are the requirements: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 1. Previous Semester Grades {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 2. Scholarship Card {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 3. Filled-up Form of Certificate of Good Standing {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 4. Student ID {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} Duration: 8 minutes {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} Fee/Charge: Free {'\n'} {'\n'}

              {'\t'} {'\t'} The Clerk In-charge assesses and verifies authenticity of said documents. After which, the
              Director/Clerk In-charge signs and issues the Certificate of Good Standing to the student. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Issuance of Certificate of Scholarship {'\n'} {'\n'}             
              </Text>

              {'\t'} {'\t'} Schedule of Availability: Monday – Friday, 8:00 am - 5:00 pm {'\n'} {'\n'}

              {'\t'} {'\t'} Key Person: Clerk {'\n'} {'\n'}

              {'\t'} {'\t'} Who may avail of the service: Students who availed scholarship {'\n'} {'\n'}

              {'\t'} {'\t'} What are the requirements: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 1. Written Request for Certificate of Scholarship {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 2. Scholarship Card {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 3. School ID {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} Duration: 12 minutes {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} Fee/Charge: Free {'\n'} {'\n'}

              {'\t'} {'\t'} The Clerk In-charge assesses and verifies authenticity of said documents. After which, the
              Director/Clerk In-charge signs and issues the Certificate of scholarship to the student. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Request for Temporary Exemption from Wearing the School Uniform {'\n'} {'\n'}             
              </Text>

              {'\t'} {'\t'} Schedule of Availability: Monday – Friday, 8:00 am - 5:00 pm {'\n'} {'\n'}

              {'\t'} {'\t'} Who may avail of the service: Students {'\n'} {'\n'}

              {'\t'} {'\t'} What are the requirements: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 1. School ID {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 2. Excuse Letter on Non-wearing of School Uniform {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} Duration: 3 minutes {'\n'} {'\n'}

              {'\t'} {'\t'}The OSS Director/Clerk In-charge evaluates the excuse letter and issues Temporary Exemption Slip. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}
 
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

OSAS.propTypes = {

};

export default OSAS;