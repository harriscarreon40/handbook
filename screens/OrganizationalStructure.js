import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class OrganizationalStructure extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
            <Left>
              <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
                this.props.navigation.openDrawer()} />
            </Left>
            <Body>
              <Text>Organizational Structure</Text>
          </Body>
            <Right>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                  onPress={() => navigate('USeP Hymn')}               
                />
              </Button>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                  onPress={() => navigate('Admission')}               
                />
              </Button>
            </Right>
        </Header>
        <ScrollView>
          <ScrollView horizontal={true}>
            <Image style = {[styles.container]}
                source = {require('../images/org.png')}
              />
          </ScrollView>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
    marginTop: 40,
    marginLeft: 20,
    alignSelf: 'center',
    justifyContent: 'center'
  }
});

OrganizationalStructure.propTypes = {

};

export default OrganizationalStructure;