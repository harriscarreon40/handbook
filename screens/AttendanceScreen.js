import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class AttendanceScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Attendance</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Credit Unit')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Examinations')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} All students are obliged to attend classes regularly and punctually. Their attendance will be religiously monitored by the teacher. {'\n'} {'\n'} {'\n'}            
              
              <Text style = {{fontWeight: 'bold'}}>
                Absences {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} The rules on attendance shall be enforced in all classes. A student shall be
              dropped or failed from his/her class when the the number of class hours missed reaches
              20 percent of the total hours required by his/her course. The table below provides the
              number of absences for the 20 percent rule to apply: {'\n'} {'\n'} {'\n'}
            </Text>

            <Image style = {{flex: 1, alignSelf: 'center'}}
              source = {require('../images/tab.png')}
            />

            <Text>
              {'\n'} {'\n'} {'\n'}
              {'\t'} {'\t'} If the majority of the absences are excused, the student shall be dropped but if the
              majority of the absences are unexcused, he/she shall be given a grade of “5”. Time lost
              by late enrollment shall be considered as time lost by absence. Excuses are for the time
              missed only. All work covered by the class during the student’s absence shall be made up
              to the satisfaction of the faculty member within a reasonable period from the date of
              absence. {'\n'} {'\n'}

              {'\t'} {'\t'} Any student who, for unavoidable reasons, is absent from a class must present a
              letter of excuse from the parent or guardian stating specifically the cause of absence. This
              shall be concurred by the Guidance Counselor, Director of the Office of Student Services
              or College Dean and endorsed to the faculty member concerned before the student is
              admitted to class. {'\n'} {'\n'}

              {'\t'} {'\t'} Students suffering from a communicable disease such as sore eyes, chickenpox,
              measles, etc. will not be allowed to attend classes. In order for the absence to be excused,
              they must secure a medical certificate issued by the university clinic stating their health
              status. This will also serve as a clearance that will allow the student to attend classes
              again. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Tardiness {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Students who are late fifteen (15) minutes after the start of classes shall be
              considered absent. However, if the teacher is late by 15 minutes, the students may leave
              the classroom, unless the teacher advices the students regarding his/her delayed arrival
              beforehand. If the faculty member arrives later than 15 minutes without prior
              information given to his/her students and the latter have already left the classroom, the            
              faculty must not impose any form of sanction to his/her students for not being there
              during that particular instance. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Medical Certificate {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Students absent from classes due to illness are required to get excuse slips from
              the University/Campus Clinic. These certificates are issued to students who have gone
              for consultation in the Clinic. Illnesses attended to elsewhere should be reported to the
              Clinic within three days after the absences have been incurred. Excuse slips for the
              above illnesses as well as for other illness of which the University/Campus Clinic has no
              records are issued only after satisfactory evidences have been presented. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Attendance to University/College Activities {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} The attendance of the students to any compulsory school activity will be checked
              by the class secretary. The student shall not be allowed to attend classes in the
              succeeding meeting without presenting the duly signed paper. {'\n'} {'\n'} {'\n'} 

              <Text style = {{fontWeight: 'bold'}}>
                Maximum Residence Rule (MRR) {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Residence refers to the number of years or terms required of a student to finish a
              degree program. An undergraduate student is allowed another 50 percent of the total
              number of years required in the curriculum of his/her program for continuous academic
              years of residence in the University exclusive of his/her approved leave of absence, as the
              case may be, otherwise he/she shall not be allowed to re-enroll further in the University. {'\n'} {'\n'} 

              {'\t'} {'\t'} If a student who has not finished the academic requirements after the lapse of
              MRR and re-enrolls the courses he/she has taken shall be evaluated based on the
              curriculum in force at the time of re-enrollment. {'\n'} {'\n'} {'\n'} 

              <Text style = {{fontWeight: 'bold'}}>
                Leave of Absence {'\n'} {'\n'} 
              </Text>

              <Text style = {{fontStyle: 'italic'}}>
              {'\t'} {'\t'} Undergraduate {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Leave of absence (LOA) should be requested in a written petition to the Dean of
              the College. The petition should state the reason for which the leave is desired and should            
              specify the period of the leave. The leave should not exceed one (1) year or two (2)
              semesters. {'\n'} {'\n'} {'\n'}
            </Text>

            <Image style = {{flex: 1, alignSelf: 'center'}}
              source = {require('../images/tab2.png')}
            />

            <Text>
              {'\n'} {'\n'} {'\n'}
              {'\t'} {'\t'} If approved, the student shall inform the University/Campus Registrar and may
              re-enroll as a continuing student the next semester immediately following the period of
              his/her leave, provided that he/she has not previously applied for a Certificate of
              Honourable Dismissal or enrolled in another school. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontStyle: 'italic'}}>
              {'\t'} {'\t'} Graduate School {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} A graduate student who cannot finish within the MRR plus one-year extension
              through LOA should again enroll six (6) units of foundation courses and six (6) units of
              specialization courses, comprehensive examination and thesis writing. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>

          </View>
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

AttendanceScreen.propTypes = {

};

export default AttendanceScreen;