import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class ClassificationScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Classification of Students</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Withdrawal From The University')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Academic Calendar')}               
              />
            </Button>
          </Right>
        </Header>
        
        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} A {' '}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Regular Student {' '}
              </Text>

              is one who has organized a program of study, is duly registered for formal academic credits, and carries the full semester load called for by the curriculum for which he/she is registered in a given semester. {'\n'} {'\n'}

              {'\t'} {'\t'} An {' '}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Irregular Student {' '}
              </Text>

              is one who is registered for formal academic units but does not carry the full semester load called for by the curriculum for which he/she is registered in a given semester. {'\n'} {'\n'}

              {'\t'} {'\t'} A {' '}

              <Text style = {{fontWeight: 'bold', fontStyle: 'italic'}}>
                Non-Regular Student {' '}
              </Text>

              is one who is registered for formal academic credits but not for a degree or is registered but does or may not receive formal academic credit/s for courses taken. Classified as non-regular students are the following: {'\n'} {'\n'}

              {'\t'} {'\t'} a) Non-degree students with credits {'\n'} {'\n'}
              {'\t'} {'\t'} b) Cross registrants with credits {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

ClassificationScreen.propTypes = {

};

export default ClassificationScreen;