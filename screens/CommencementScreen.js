import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class CommencementScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Commencement and Baccalaureate Services</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Graduation Matters')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Policy on Student Records')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} Attendance of graduating students in the general commencement exercises is
              optional. Graduating students who choose not to participate in the general
              commencement exercises must so inform their respective Deans or their duly designated
              representatives at least ten (10) days before the commencement exercise. {'\n'} {'\n'}

              {'\t'} {'\t'} Graduating students who are absent from the general commencement exercises
              will obtain their diplomas, or certificates and transcripts or records from the Office of the
              University Registrar provided that they comply with all other requirements and present
              the receipt of payment of the graduation fees and student’s clearance. {'\n'} {'\n'}

              {'\t'} {'\t'} Attendance at the commencement exercises is not compulsory for the awarding of
              corresponding undergraduate certificate or diploma. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Academic Costumes {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Candidates for graduation with degrees or titles, which require no less than four
              years of collegiate instruction, are required to wear academic costumes during the
              baccalaureate mass/services and commencement exercises in accordance with the rules
              and regulations of the University. {'\n'} {'\n'}

              {'\t'} {'\t'}Only graduates of Baccalaureate, Masters and Doctorate programs are installed
              with their respective “Hoods” during the commencement exercises. The system of
              hooding depends upon the plan of the graduation program committee in accordance with
              existing rules approved by the University Council. Graduates of diploma, undergraduate
              certificate and graduate certificate courses may also wear academic gown during the
              commencement exercises provided they are in unison. They may be allowed to install
              something but strictly not of a hood type. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

CommencementScreen.propTypes = {

};

export default CommencementScreen;