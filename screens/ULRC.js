import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class ULRC extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>ULRC</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('University Clinic')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('OUR')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              <Text style = {{fontWeight: 'bold'}}>
                Service Hours {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} The Learning Resource Center is open 9 to 12 hours a day from Monday to Saturday, NO NOON BREAK. {'\n'} {'\n'} {'\n'}
            </Text>

            <Image
              source = {require('../images/tabs.png')}
            />

            <Text style = {{fontWeight: 'bold'}}>
              {'\n'} {'\n'} Library Services {'\n'} {'\n'}               
            </Text>

            <Text>
              {'\t'} {'\t'} a) References Services – The Librarian renders guidance and assistance to the readers in locating the information. {'\n'} {'\n'}

              {'\t'} {'\t'} b) Circulation Services – The Library provides assistance in the issuance and retrieval of library resources loaned by the users. {'\n'} {'\n'}

              {'\t'} {'\t'} c) Periodical Services – It provides the researchers current issues of newspaper, journals and magazines, in print and online. {'\n'} {'\n'}

              {'\t'} {'\t'} d) Library Orientation and Instruction Services – It provides orientation and
              instruction to all freshmen and transferee students in the graduate and
              undergraduate programs. {'\n'} {'\n'}

              {'\t'} {'\t'} e) Internet Services – It provides access to online resources (Wi-Fi and wired). {'\n'} {'\n'}

              {'\t'} {'\t'} f) Online Resources - It provides access to e-books and e-journals. The University
              Library is a recipient of the Philippine E-Library project. It is a collaborative
              project of the National Library of the Philippines (NLP), University of the
              Philippines (UP), Department of Science and Technology (DOST), Department of
              Agriculture (DA), and the Commission on Higher Education (CHED). Available
              resources include: {'\n'} {'\n'}

              {'\t'} {'\t'} g) Union catalog of the five partners; {'\n'} {'\n'}

              {'\t'} {'\t'} h) Digitized Filipiniana materials including theses and dissertations; {'\n'} {'\n'}

              {'\t'} {'\t'} i) Special collection/researches of the five partners; and, {'\n'} {'\n'}

              {'\t'} {'\t'} j) Online resources/subscription to electronic databases. {'\n'} {'\n'}

              {'\t'} {'\t'} k) Audio-Visual Services – It provides assistance in the use of multimedia materials such as; CD-ROMs, slides, facilities and equipment. {'\n'} {'\n'}

              {'\t'} {'\t'} l) Academic Writing Services– It assists undergraduate and graduate students working on research papers. {'\n'} {'\n'}

              {'\t'} {'\t'} m) Information Dissemination – It provides information on its resources and services
              through its website, bulletin boards, announcements, user’s guide, brochures,
              newsletter, etc. {'\n'} {'\n'}

              {'\t'} {'\t'} n) The University’s On-Line Public Access Catalog (OPAC) - This is an online
              database or bibliography of a library collection held by the University of
              Southeastern Philippines (USeP) Libraries. Users may search for books, journals,
              thesis & dissertations, movies, music, or anything that is in the library. {'\n'} {'\n'} {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} To use the OPAC: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 1. Access www.opac.usep.edu.ph {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 2. Type in the topic and start searching the database. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} 3. Locate the book in the shelves and inform the library staff in case you want to borrow the book for home use. {'\n'} {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Library Users {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} The library may be used by: {'\n'} {'\n'}

              {'\t'} {'\t'} 1. The members of the University of Southeastern Philippines constitutes the following: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} a. Members of the Administrative Council {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} b. Members of the Academic Council {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} c. Faculty {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} d. Non-teaching staff {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} e. Student {'\n'} {'\n'} {'\n'}

              {'\t'} {'\t'} 2. Council members of the Davao Colleges and Universities’ Network (DACUN)
              have access to print collections free of charge but should follow the library’s
              existing policy. {'\n'} {'\n'}

              {'\t'} {'\t'} 3. Non-members of USeP community may be allowed, for a fee, to use the library.
              They are expected to follow the library’s rules and regulations. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                USeP E-Library Card {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} New Students are required to pass the following to the University Library: {'\n'} {'\n'} {'\n'}

              {'\t'} {'\t'} • 2 pieces of 1x1 picture (identical with plain background) {'\n'} {'\n'}

              {'\t'} {'\t'} • Certificate of Registration (C.O.R) {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Lost E-Library Card {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Students are asked to secure the following requirements in the case of a lost ELibrary Card: {'\n'} {'\n'} {'\n'}

              {'\t'} {'\t'} • Affidavit of Loss {'\n'} {'\n'}

              {'\t'} {'\t'} • 2 pieces of 1x1 picture (identical with plain background) {'\n'} {'\n'}

              {'\t'} {'\t'} • Php30.00 replacement fee {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Rules in Borrowing Books from the Library {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Students are asked to observe and respect the following rules of borrowing books presented by the library: {'\n'} {'\n'} {'\n'}

              {'\t'} {'\t'} • Periodicals – For library use only and may be loaned for 15 minutes for photocopying purposes. {'\n'} {'\n'}

              {'\t'} {'\t'} • Undergraduate Books – Two (2) books may be loaned for three (3) days only
              excluding Saturdays, Sundays and Holidays. {'\n'} {'\n'}

              {'\t'} {'\t'} • Reserved Books – One (1) book for library use and may be loaned for 15 minutes for photocopying purposes. {'\n'} {'\n'}

              {'\t'} {'\t'} • General Reference - One (1) book for library use and may be loaned for 15 minutes for photocopying purposes. {'\n'} {'\n'}

              {'\t'} {'\t'} • Filipiniana Books - One (1) book for library use and may be loaned for 15 minutes for photocopying purposes. {'\n'} {'\n'}

              {'\t'} {'\t'} • Graduate Books - Two (2) books may be loaned for three (3) days only excluding Saturdays, Sundays and Holidays. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Overdue Books {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Students are asked to be aware that upon borrowing the University Library books
              they are subject to the following fines and penalties if they fail to return the books
              on due dates: {'\n'} {'\n'}

              {'\t'} {'\t'} • Circulation Books (graduate and undergraduate) – P 1.00 per day (excluding holidays, Sundays, and Saturdays FOR UNDERGRADUATE ONLY) {'\n'} {'\n'}

              {'\t'} {'\t'} • Reserved Books / General References / Filipiniana - P 1.50 after 15 mins and P
              0.50 every hour (including holidays, Sundays, and Saturdays FOR
              UNDERGRADUATE ONLY) {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Lost Borrowed Book {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Students who lose borrowed books are asked to do the following: {'\n'} {'\n'}

              {'\t'} {'\t'} 1. Report the incident immediately to the librarian. {'\n'} {'\n'}

              {'\t'} {'\t'} 2. Replace the lost material with: {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} a. The same title or if not available in the market, replace with related
              title with a recommendation from the librarian. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} b. Latest edition of the lost material {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Policy Concerning Signing of Clearance {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} The University Librarian schedules the signing of library clearance after the final
              examination period every semester including summer classes. This is per scheduled by
              USeP-LRC and approved by the Vice President for Academic Affairs. The signing of
              clearance is scheduled per college to accommodate the student in an orderly manner. {'\n'} {'\n'}

              {'\t'} {'\t'} The following requirements are needed: {'\n'} {'\n'}

              {'\t'} {'\t'}  • Individual clearance form {'\n'} {'\n'}

              {'\t'} {'\t'}  • Library card with 1 x 1 attached ID picture as required.: {'\n'} {'\n'}

              {'\t'} {'\t'}  A sanction, determined by the University Librarian, is imposed for late
              submission of clearance. The schedule for the signing of clearance should be strictly
              followed to make way for inventory of library books and other materials as well as the
              equipment, processing and cataloging of books, and distribution. The Commission On
              Audit (COA) strictly requires the library to submit the said documents before the summer
              period ends. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Policy for Non-USeP Researchers {'\n'} {'\n'}               
              </Text>

              {'\t'} {'\t'} Researchers from other schools can avail of the USeP library collections after
              paying an amount of fifty pesos (Php50.00) per day as per Board Resolution #2437
              s. 1995. {'\n'} {'\n'}

              {'\t'} {'\t'} Non-USeP researchers are not allowed to borrow library materials and should
              only use these library materials within the library premises.
              They should follow the control procedures by leaving their bag, attaché case, envelopes
              at the baggage counter. They should submit these for inspection before leaving the
              library. The member institutions of the Davao Colleges and Universities' Network
              (DACUN) are given free access to the library resources. Lastly, non-USeP researchers
              are only allowed to photocopy the materials after seeking permission from the librarian. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}             
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

ULRC.propTypes = {

};

export default ULRC;