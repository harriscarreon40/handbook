import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class GraduationMatters extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Graduation Matters</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Recognition of High Scholastic Achievement')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Commencement and Baccalaureate Services')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} A student shall be recommended for graduation only after he/she has satisfied all
              academic and other requirements prescribed thereto and has completed at least one (1)
              year of residence work immediately prior to graduation. Residence work may be
              extended to a longer period by the Dean of the College through their respective
              department chair. {'\n'} {'\n'}

              {'\t'} {'\t'} Graduation of students who began their studies under previous curricula shall be governed by the following rules: 
              {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} a) Those who have completed all the requirements of the curriculum but did not
              apply for, nor were granted the corresponding degree or title shall have their
              graduation approved as of the date they should have originally graduated. {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} b) Students who have completed all but two (2) or three (3) courses required by the
              curriculum shall be made to follow either the revised curriculum or the curriculum
              enforced from the time they first enrolled in the University to the present. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Guidelines in Connection with Graduation {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} All candidates for graduation must have their deficiencies made up and their
              records cleared at least one week before the end of their last semester/term in the
              program, with the exception of those courses in which the student is currently enrolled
              during that semester. {'\n'} {'\n'}

              {'\t'} {'\t'} The requirements for graduation include the completion of all academic as well as
              non-academic requirements such as submission of bound copies of the
              dissertation/thesis/project study and the like, if required, on or before the deadline
              prescribed hereinafter. Otherwise, students concerned should not be included in the final
              list of candidates for graduation as of the end of the semester/term. {'\n'} {'\n'}

              {'\t'} {'\t'} If however, some graduation requirements are completed beyond the deadline, the
              student must register during the succeeding semester or summer in order to be considered
              a candidate for graduation as of the end of that semester/summer. The deadlines for              
              completion, specific dates of which shall be stipulated in the University Academic
              Calendar, of the requirements for graduation are:{'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} a) For those graduating as of the end of summer, the deadline is the day
              before the first day of regular registration for the first semester. {'\t'} {'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} b) For those graduating as of the end of the first semester, the deadline is the
              day before the first day of regular registration for the second semester.{'\n'} {'\n'}

              {'\t'} {'\t'} {'\t'} {'\t'} c) For those graduating as of the end of the second semester, the deadline is
              one week before the date of graduation.{'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Formal Application for Graduation {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} A graduating student must file formal application with the Office of the
              University Registrar as candidate for graduation upon enrolment or within three (3)
              weeks after enrolment on his/her last semester/term in College. This information shall
              serve as the basis for identifying candidates for graduation so that their records can be
              checked early enough.  {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Requirements for Graduation {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} Students who have completed all the academic requirements for their respective
              degrees may be recommended for graduation even if they have not processed their
              clearance. However, the granting of honorable dismissal and the issuance of the
              transcript of records, diploma and other documents shall be withheld pending
              submission of clearance by the student. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Presentation of Candidates for Graduation to the Academic Council {'\n'} {'\n'} 
              </Text>

              {'\t'} {'\t'} After the filing of application for graduation, the Office of the University
              Registrar shall conduct a faculty meeting in all colleges/campuses to present and
              scrutinize the academic records of all their graduating students. After that, the Office of
              the University Registrar must submit the complete list of candidates for graduation,
              including the list of candidates for academic honors to the Vice President for Academic
              Affairs for presentation to the University Academic Council.{'\n'} {'\n'}  {'\n'} {'\n'} {'\n'} {'\n'}             
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

GraduationMatters.propTypes = {

};

export default GraduationMatters;