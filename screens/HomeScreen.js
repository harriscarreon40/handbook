import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, ScrollView } from 'react-native'
import { Header, Left ,Right, Body, Icon, Container, Button} from 'native-base'



class HomeScreen extends Component {
  render () {
    const {navigate} = this.props.navigation;
    return (
      <Container>
        <View style = {[styles.container]}>
          <Header style = {{backgroundColor: "#ECF0F1"}}>
            <Left>
              <Icon style = {{color: "#8B0000"}} name = "menu" onPress = {()=>
                this.props.navigation.openDrawer()} />
            </Left>
            <Body>
              <Text>Home</Text>
            </Body>
            <Right>
              <Button transparent>
                <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                  onPress={() => navigate('Student Pledge')}               
                />
              </Button>
            </Right>
          </Header>

          <ScrollView>
            <ScrollView horizontal = {true}>
              <Image
                source = {require('../images/handbook_cover.jpg')}
              />
            </ScrollView>
          </ScrollView>

        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    }
  });
  

HomeScreen.propTypes = {

};

export default HomeScreen;