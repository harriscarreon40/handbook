import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class WithdrawalScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Withdrawal from the University</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Academic Courses')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Classification of Students')}               
              />
            </Button>
          </Right>
        </Header>
        
        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
            {'\t'} {'\t'} A student reserves the right to withdraw himself/herself from the roster of the
            University. He/She first accomplishes an official withdrawal/ dropping form from the
            OUR. He/She then surrenders his/her ID card and pays the fees to defray expenses in the
            preparation of his/her transfer credentials and the forwarding of records to the college or
            university to which he/she plans to transfer. A student will be given a certificate of
            eligibility to transfer that will allow him/her for admission to another school, provided all
            debts in the University has been settled. {'\n'} {'\n'}

            {'\t'} {'\t'} Students who withdraw from the University without the formal withdrawal
            process there from will have their registration privileges curtailed or entirely withdrawn.
            Furthermore, they will be liable for unpaid authorized fees. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>
          </View>
        </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

WithdrawalScreen.propTypes = {

};

export default WithdrawalScreen;