import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class StudentConduct extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Student Conduct</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('School Identification Card')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Basis of Discipline')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text>
              {'\t'} {'\t'} Every student is at all times expected to observe the law of the land, the rules and
              regulations of the University and all standards of good society. In addition to these
              minimum requirements, every good student always acts with fairness, tolerance and
              moderation with due regard for the opinions and feelings of others. He/She should bear in
              mind that education stands for broadness of views, appreciation of principles,
              consideration of the feelings of others, and a sympathetic understanding of the needs of
              others. {'\n'} {'\n'} {'\n'}
            </Text>

            <Text style = {{fontWeight: 'bold', textAlign: 'center'}}>
              Norms of Conduct {'\n'} {'\n'}               
            </Text>

            <Text style = {{fontWeight: 'bold'}}>
              Moral Character {'\n'} {'\n'}               
            </Text>

            <Text>
              A student is imbued with moral character when: {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She has learned to act, live and think as a person whose values, attitudes and
              convictions are in accord with the Universal Ethical Norms of Right Reason and
              the accepted values and approved levels of conduct in the society where he/she
              lives; {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She is honest to himself, accepts his shortcomings, and strives to improve and change; {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She is fair and just in his/her dealings with his/her fellowmen; {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She lives by the precepts of love, justice, compassion, and concern for others; and, {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She respects the rights of others, as he/she would want his/her rights to be respected. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Personal Discipline {'\n'} {'\n'}               
              </Text>

              A student is imbued with personal discipline when: {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She devotes himself/herself to the fulfillment of his/her obligations and
              considers rights as means to or rewards for the same; {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She learns to forego the enjoyment of certain rights and privileges that others
              more needy may benefit more for the greater good of society; {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She resolves his/her problems and conflicts without prejudicing others; {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She is tolerant of others, and humble to accept what is better than his/her; {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She has developed temperance and propriety in words and action, especially
              against vices, e.g. gambling, drinking liquor, drugs, sexual excesses and
              aberrations, etc. and, {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She endeavors that right reason guides and controls his/her life, actions and emotions. {'\n'} {'\n'} {'\n'}

              <Text style = {{fontWeight: 'bold'}}>
                Civic Conscience and Patriotism {'\n'} {'\n'}               
              </Text>

              A student is imbued with civic conscience and patriotism when: {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She devotes himself to the growth and development of the Philippines; {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She puts the welfare of the entire country above his personal, family and religious interest; {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She respects and obeys all duly constituted authorities and laws, rules and regulations; {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She settles all disputes, problems and conflicts through peaceful means; {'\n'} {'\n'}

              {'\t'} {'\t'} • He/She respects the Philippine Flag as the symbol of our country. {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}

            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

StudentConduct.propTypes = {

};

export default StudentConduct;