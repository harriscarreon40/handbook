import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Header, Left ,Right, Body, Icon, Button} from 'native-base'

class RecognitionScreen extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header style = {{backgroundColor: "#ECF0F1"}}>
          <Left>
            <Icon style = {{color: "#8B0000"}} name = "menu"  onPress = {()=>
              this.props.navigation.openDrawer()} />
          </Left>
          <Body>
          <Text>Recognition of High Scholastic Achievement</Text>
        </Body>
          <Right>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-back' 
                onPress={() => navigate('Grading System')}               
              />
            </Button>
            <Button transparent>
              <Icon style = {{color: "#8B0000"}} name='arrow-forward' 
                onPress={() => navigate('Graduation Matters')}               
              />
            </Button>
          </Right>
        </Header>

        <ScrollView>
          <View style = {[styles.container]}>
            <Text style = {{fontWeight: 'bold'}}>
              Dean's List {'\n'} {'\n'} 
            </Text>

            <Text>
            {'\t'} {'\t'} At the end of every semester, all colleges/academic units prepare a “Dean’s List”
            which recognizes students in their respective colleges/academic units who achieved
            outstanding performance in academics. The criteria for inclusion in the "Dean's List" are
            to be determined by each college/academic unit. A student must have an academic load
            of at least eighteen (18) units or the normal load prescribed in the curriculum in that
            semester for him/her to qualify for the recognition. The "Dean's list" is published in the
            school publication or posted in any conspicuous place in the college/campus immediately
            after a semester ends. The college/academic unit may give a certificate indicating
            academic excellence to students who received this recognition.{'\n'} {'\n'} {'\n'}

            <Text style = {{fontWeight: 'bold'}}>
              Graduation with Honors {'\n'} {'\n'} 
            </Text>

            {'\t'} {'\t'} a) Undergraduate students who completed their courses with the following range of
            general weighted average computed to the second decimal place will graduate
            with honors: {'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} Summa Cum Laude...... 1.00 - 1.20 {'\n'}
            {'\t'} {'\t'} {'\t'} {'\t'} Magna Cum Laude........ 1.21 - 1.45 {'\n'}
            {'\t'} {'\t'} {'\t'} {'\t'} Cum Laude..................... 1.46 - 1.75 {'\n'} {'\n'} 
            
            {'\t'} {'\t'} b) In the case of candidates for graduation with honors, the following special rules shall apply:{'\n'} {'\n'}
            
            {'\t'} {'\t'} {'\t'} {'\t'} 1) Candidates had enrolled no less than the normal load prescribed in the
            curriculum during all the semesters of stay in the university; {'\n'} {'\n'}
            
            {'\t'} {'\t'} {'\t'} {'\t'} 2) Candidates had completed the required academic units within the
            approved prescribed period of study in continuous enrollment except when
            his/her absence is caused by serious illness or accident as certified by
            government physician. An LOA for one year maybe granted to student to
            qualify him/her for honors. {'\n'} {'\n'}
            
            {'\t'} {'\t'} {'\t'} {'\t'} 3) Candidates had not incurred dropped, incomplete or failed subjects; {'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} 4) The General Weighted Average (GWA) computation includes all grades
            obtained in all academic subjects under the curriculum pursued by the
            candidate except NSTP; {'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} 5) For transferees, the following additional conditions apply: {'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} {'\t'} {'\t'} i. That fifty (50) percent or more of the total number of
            academic units required for graduation had been completed
            in the University; {'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} {'\t'} {'\t'} ii. That candidate had acquired residence work in the
            University for a period of at least two years for four-year
            courses and three years for five-year courses immediately
            prior to graduation; {'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} {'\t'} {'\t'} iii. That candidate had not incurred dropped or failed subjects
            from any of the schools previously attended;{'\n'} {'\n'}

            {'\t'} {'\t'} {'\t'} {'\t'} {'\t'} {'\t'} iv. That in the computation of the GWA, the rating system of
            previous institution will be applied in all accredited/validated subjects required in the curriculum
            pursued by the candidate.{'\n'} {'\n'}
            </Text>

            <Image style = {{flex: 1, alignSelf: 'center'}}
              source = {require('../images/formula.png')}
            />

            <Text>
            {'\n'} {'\n'} {'\n'} {'\n'} {'\n'} {'\n'}
            </Text>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginTop: 40
  }
});

RecognitionScreen.propTypes = {

};

export default RecognitionScreen;