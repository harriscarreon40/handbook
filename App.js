import React from 'react';
import { StyleSheet, SafeAreaView, ScrollView } from 'react-native';
import {createAppContainer, createDrawerNavigator, DrawerItems } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import StudentPledgeScreen from './screens/StudentPledgeScreen'
import MessageOfThePresident from './screens/MessageOfThePresident'
import Preface from './screens/Preface'
import UsepVMGC from './screens/UsepVMGC'
import UniversityHistory from './screens/UniversityHistory'
import PresentTimes from './screens/PresentTimes'
import UniversitySymbols from './screens/UniversitySymbols';
import UsepHymn from './screens/UsepHymn';
import OrganizationalStructure from './screens/OrganizationalStructure';
import AdmissionScreen from './screens/AdmissionScreen'
import RegistrationScreen from './screens/RegistrationScreen';
import AcademicCourses from './screens/AcademicCourses';
import WithdrawalScreen from './screens/WithdrawalScreen';
import ClassificationScreen from './screens/ClassificationScreen';
import AcademicCalendar from './screens/AcademicCalendar';
import ClassSize from './screens/ClassSize';
import ScheduleOfClass from './screens/ScheduleOfClass';
import SpecialClasses from './screens/SpecialClasses';
import CreditUnit from './screens/CreditUnit';
import AttendanceScreen from './screens/AttendanceScreen';
import ExaminationScreen from './screens/ExaminationScreen';
import GradingSystem from './screens/GradingSystem';
import RecognitionScreen from './screens/RecognitionScreen';
import GraduationMatters from './screens/GraduationMatters';
import CommencementScreen from './screens/CommencementScreen';
import PolicyScreen from './screens/PolicyScreen';
import DelinquencyScreen from './screens/DelinquencyScreen';
import HonorableDismissal from './screens/HonorableDismissal';
import FeesScreen from './screens/FeesScreen';
import SchoolUniforms from './screens/SchoolUniforms';
import SchoolID from './screens/SchoolID';
import StudentConduct from './screens/StudentConduct';
import BasisOfDiscipline from './screens/BasisOfDiscipline';
import CommitteeScreen from './screens/CommitteeScreen';
import UGTOScreen from './screens/UGTOScreen';
import UniversityClinic from './screens/UniversityClinic';
import ULRC from './screens/ULRC';
import OUR from './screens/OUR';
import OSAS from './screens/OSAS';
import FinancialAid from './screens/FinancialAid';
import StudentGovernment from './screens/StudentGovernment';
import StudentDuties from './screens/StudentDuties';
import Appendices from './screens/Appendices';


const DrawerComponents = (props) => (
  <SafeAreaView style = {{flex: 1, backgroundColor: "#ECF0F1"}}>
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </SafeAreaView>
)

const AppDrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    "Student Pledge": {
      screen: StudentPledgeScreen,
    },
    "Message of the President": {
      screen: MessageOfThePresident,
    },
    Preface: {
      screen: Preface,
    },
    "USeP VMGC": {
      screen: UsepVMGC,
    },
    "University History": {
      screen: UniversityHistory,
    },
    "Present Times": {
      screen: PresentTimes,
    },
    "University Symbols": {
      screen: UniversitySymbols,
    },
    "USeP Hymn": {
      screen: UsepHymn,
    },
    "Organizational Structure": {
      screen: OrganizationalStructure,
    },
    Admission: {
      screen: AdmissionScreen,
    },
    Registration: {
      screen: RegistrationScreen,
    },
    "Academic Courses": {
      screen: AcademicCourses,
    },
    "Withdrawal From The University": {
      screen: WithdrawalScreen,
    },
    "Classification of Students": {
      screen: ClassificationScreen,
    },
    "Academic Calendar": {
      screen: AcademicCalendar,
    },
    "Class Size": {
      screen: ClassSize,
    },
    "Schedule of Class": {
      screen: ScheduleOfClass,
    },
    "Special Classes": {
      screen: SpecialClasses,
    },
    "Credit Unit": {
      screen: CreditUnit,
    },
    Attendance: {
      screen: AttendanceScreen,
    },
    Examinations: {
      screen: ExaminationScreen,
    },
    "Grading System": {
      screen: GradingSystem,
    },
    "Recognition of High Scholastic Achievement": {
      screen: RecognitionScreen,
    },
    "Graduation Matters": {
      screen: GraduationMatters,
    },
    "Commencement and Baccalaureate Services": {
      screen: CommencementScreen,
    },
    "Policy on Student Records": {
      screen: PolicyScreen,
    },
    "Scholastic Delinquency": {
      screen: DelinquencyScreen,
    },
    "Honorable Dismissal": {
      screen: HonorableDismissal,
    },
    "School Tuition and Miscellaneous Fees": {
      screen: FeesScreen,
    },
    "School Uniforms": {
      screen: SchoolUniforms,
    },
    "School Identification Card": {
      screen: SchoolID,
    },
    "Student Conduct": {
      screen: StudentConduct,
    },
    "Basis of Discipline": {
      screen: BasisOfDiscipline,
    },
    "Committee on Student Discipline": {
      screen: CommitteeScreen,
    },
    UGTO: {
      screen: UGTOScreen,
    },
    "University Clinic": {
      screen: UniversityClinic,
    },
    ULRC: {
      screen: ULRC,
    },
    OUR: {
      screen: OUR,
    },
    OSAS: {
      screen: OSAS,
    },
    "Financial Aid": {
      screen: FinancialAid,
    },
    "Student Government, Campus Organizations and Publication": {
      screen: StudentGovernment,
    },
    "Student Duties and Responsibilities": {
    screen: StudentDuties,
    },
    Appendices: {
      screen: Appendices,
    }
  },
  {
    contentComponent: DrawerComponents
  }
);

const AppContainer = createAppContainer(AppDrawerNavigator);


export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}